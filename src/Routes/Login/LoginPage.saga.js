import loginSaga from './sagas/Login.saga';
import resetPasswordSaga from './sagas/resetPwd.saga';

export default [
  loginSaga, resetPasswordSaga,
];
