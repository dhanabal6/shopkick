import { BASE_URL } from '../../constants/urls';

export const ACTION_LOGIN_REQUEST = 'shopkick/Login/login_request';
export const ACTION_RESET_PASSWORD_REQUEST = 'shopkick/Login/resetPassword_request';

export const URL_LOGIN = `${BASE_URL}/users/login`;
export const URL_RESET_PWD = `${BASE_URL}/users/forgotpwd`;
export const ACTION_LOGIN_ERROR = 'cp/LoginPage/LOGIN_ERROR';
export const ACTION_LOGIN_SUCCESS = 'cp/LoginPage/LOGIN_SUCCESS';
