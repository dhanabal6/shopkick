import {
  ACTION_LOGIN_ERROR,
  ACTION_LOGIN_REQUEST,
  ACTION_LOGIN_SUCCESS,
  ACTION_RESET_PASSWORD_REQUEST,
} from './LoginPageConstants';

export function logIn(email, password) {
  return {
    type: ACTION_LOGIN_REQUEST,
    payload: { email, password },
  };
}

export function resetPassword(email) {
  return {
    type: ACTION_RESET_PASSWORD_REQUEST,
    payload: { email },
  };
}

export const onLoginFailure = error => ({
  type: ACTION_LOGIN_ERROR,
  payload: error,
});

export const onLoginSuccess = response => ({
  type: ACTION_LOGIN_SUCCESS,
  payload: { response },
});
