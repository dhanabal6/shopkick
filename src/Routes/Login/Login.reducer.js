import { fromJS } from 'immutable';
import { createReducerFromObject } from '../../utils/reducerUtils';
import { ACTION_LOGIN_SUCCESS } from './LoginPageConstants';

export const initialState = fromJS({
  error: null,
  loggedIn: !!(JSON.parse(localStorage.getItem('shopkick-login-remember')) && JSON.parse(localStorage.getItem('shopkick-accessToken-remember'))),
  data: {
    userId: null,
    userSessionToken: null,
  },
});

export const userDataOnLogin = (state, payload) => {
  return fromJS({
    error: '',
    loggedIn: true,
    data: {
      userId: payload.response.id,
      userSessionToken: payload.response.token,
    },
  });
};

const reducerFunctions = {
  [ACTION_LOGIN_SUCCESS]: userDataOnLogin,
};

const loginReducer = createReducerFromObject(reducerFunctions, initialState);
export default loginReducer;
