import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Anchor from '../../components/Anchor/Anchor';
import Button from '../../components/Button/Button';
import Checkbox from '../../components/Checkbox/Checkbox';
import Input from '../../components/Input/Input';
import { logIn, resetPassword } from './Login.actions';
import styles from './Login.css';

class Login extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
  };
  static defaultProps = {
    dispatch: f => f,
  };

  constructor() {
    super();
    this.state = {
      rememberUser: false,
      email: '',
      password: '',
      validEmail: true,
      validPassword: true,
    };
    this.getForgotPasswordLink = this.getForgotPasswordLink.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getCookie = this.getCookie.bind(this);
  }

  componentWillMount() {
    if (JSON.parse(localStorage.getItem('shopkick-login-remember')) && JSON.parse(localStorage.getItem('shopkick-accessToken-remember'))) {
      // window.location = '/';
    }
    const user = this.getCookie('shopkick-cp-Email');
    const pwd = this.getCookie('shopkick-cp-Password');
    this.state = {
      email: user,
      password: pwd,
      validEmail: true,
      validPassword: true,
      rememberUser: JSON.parse(localStorage.getItem('shopkick-login-remember')),
    };
  }

  onChange(type, val) {
    const validate = (type) => {
      if (type === 'email' && !this.state.validEmail) {
        this.validateEmail(val);
      }
      if (type === 'password') {
        this.validatePassword(val);
      }
    };
    this.setState({
      [type]: val,
    }, validate(type, val));
  }

  getCookie(cname) {
    const name = `${cname}=`;
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  getForgotPasswordLink(e) {
    e.preventDefault();
    if (this.state.validEmail) {
      this.props.dispatch(resetPassword(this.state.email));
    } else {
      this.validateEmail(this.state.email);
    }
  }

  validatePassword(val) {
    if (val && val.trim() !== '') {
      this.setState({ validPassword: true });
    } else {
      this.setState({ validPassword: false });
    }
  }

  validateEmail(value) {
    const emailPattern = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i;
    const email = value.trim();
    if (email.match(emailPattern) && value.trim() !== '') {
      this.setState({ validEmail: true });
    } else {
      this.setState({ validEmail: false });
    }
  }

  login() {
    this.validatePassword(this.state.password);
    if (this.state.validEmail && this.state.password !== '') {
      // setting cookies on remember
      const setCookie = (cname, cvalue, exdays) => {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        const expires = `expires=${d.toGMTString()}`;
        document.cookie = `${cname}=${cvalue};${expires};path=/`;
      };

      if (this.state.rememberUser && this.state.email && this.state.password) {
        setCookie('shopkick-cp-Email', this.state.email, 30);
        setCookie('shopkick-cp-Password', this.state.password, 30);
      }

      if (!this.state.rememberUser && this.state.email && this.state.password) {
        setCookie('shopkick-cp-Email', '', 30);
        setCookie('shopkick-cp-Password', '', 30);
      }
      this.props.dispatch(logIn(this.state.email, this.state.password));
      this.state.rememberUser ? localStorage.setItem('shopkick-login-remember', true) : localStorage.setItem('shopkick-login-remember', false);
      this.setState({ password: '' });
    }
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <div className={styles.container}>
          <Anchor
            href="#"
            className={styles.logo}
            onClick={(e) => {
              e.preventDefault();
            }}
          />
          <div className={styles.fieldsWrapper}>
            <Input
              autoFocus
              type={'text'}
              placeHolder={'Enter your email ID'}
              value={this.state.email}
              name={'email'}
              error={!this.state.validEmail}
              className={styles.inputField}
              onEnter={this.login}
              onBlur={(e, value) => this.validateEmail(value)}
              onChange={(e, value) => this.onChange('email', value)}
              message={this.state.email === '' ? 'Provide your email Id' : 'Check your email ID'}
            />
            <Input
              type={'password'}
              placeHolder={'Enter your password'}
              value={this.state.password}
              name={'password'}
              error={!this.state.validPassword}
              className={styles.inputField}
              onEnter={this.login}
              onChange={(e, value) => this.onChange('password', value)}
              message={this.state.password === '' ? 'Provide your password' : 'Check your password'}
            />
            <Anchor
              href="#"
              className={styles.link}
              onClick={e => this.getForgotPasswordLink(e)}
              child={'Forgot password?'}
            />
          </div>
          <Button
            className={styles.loginButton}
            text={'Login'}
            onClick={this.login}
          />
          <div
            className={styles.remember}
            onClick={() => this.setState({ rememberUser: !this.state.rememberUser })}
          >
            <Checkbox checked={this.state.rememberUser} />
            Remember me on this computer
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Login);

