import { call, fork, takeLatest } from 'redux-saga/effects';
import request from '../../../utils/request';
import { URL_RESET_PWD, ACTION_RESET_PASSWORD_REQUEST } from '../LoginPageConstants';
import { showMessageAlert } from '../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../index';

export function* resetPwd(action) {
  try {
    yield call(request, URL_RESET_PWD, {
      method: 'POST',
      body: {
        user_email: action.payload.email,
      },
    },
    {
      onSuccess() {
        store.dispatch(showMessageAlert({
          message: 'Reset Password link sent to your email!',
          visible: true,
          color: '#23e560',
        }));
      },
      onError() {
        store.dispatch(showMessageAlert({
          message: 'Error occured, Please try again later',
          visible: true,
          color: '#e53e3d',
        }));
        return false;
      },
    }, false);
  } catch (error) {
    store.dispatch(showMessageAlert({
      message: 'Error occured, Please try again later',
      visible: true,
      color: '#e53e3d',
    }));
  }
}

export default function* resetPasswordSaga() {
  yield fork(takeLatest, ACTION_RESET_PASSWORD_REQUEST, resetPwd);
}

