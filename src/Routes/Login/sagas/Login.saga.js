import { call, fork, put, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount, incrementLoaderCount } from '../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../index';
import request from '../../../utils/request';
import { onLoginFailure, onLoginSuccess } from '../Login.actions';
import { ACTION_LOGIN_REQUEST, URL_LOGIN } from '../LoginPageConstants';

export function* loginUser(action) {
  try {
    yield put(incrementLoaderCount());
    yield call(request, URL_LOGIN,
      {
        method: 'POST',
        body: {
          email: action.payload.email,
          password: action.payload.password,
        },
      },
      {
        onSuccess(response) {
          if (response && response.token) {
            localStorage.setItem('shopkick-accessToken-remember', JSON.stringify(response.token));
          }
          localStorage.setItem('userId', response.id);
          store.dispatch(onLoginSuccess(response));
          store.dispatch(decrementLoaderCount());
        },
        onError() {
          store.dispatch(decrementLoaderCount());
          localStorage.setItem('shopkick-accessToken-remember', JSON.stringify(null));
          store.dispatch(showMessageAlert({
            message: 'Invalid Credentials',
            visible: true,
            color: '#e53e3d',
          }));
          store.dispatch(onLoginFailure());
          return false;
        },
      }, false);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* loginSaga() {
  yield fork(takeLatest, ACTION_LOGIN_REQUEST, loginUser);
}

