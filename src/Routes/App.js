import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import Loader from '../containers/Loader/Loader';
import { loaderCountSelector } from '../containers/Loader/Loader.selector';
import MessageAlert from '../containers/MessageAlert/MessageAlert';
import AdminPanel from './Home/AdminPanel/AdminPanel';
import Dashboard from './Home/Dashboard/Dashboard';
import NewCampaign from './Home/NewCampaign/NewCampaign';
import Login from './Login/Login';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedin: false,
    };
  }

  componentWillMount() {
    const token = localStorage.getItem('shopkick-accessToken-remember');
    // if token is present
    // validate token
    if (token) {
      this.setState({
        loggedin: true,
      });
    }
    // token not present
    else {
      this.setState({
        loggedin: false,
      });
    }
  }

  componentWillReceiveProps() {
    const token = localStorage.getItem('shopkick-accessToken-remember');
    // if token is present
    // validate token
    if (token) {
      this.setState({
        loggedin: true,
      });
    }
    // token not present
    else {
      this.setState({
        loggedin: false,
      });
    }
  }

  render() {
    const { showMessageAlert, loaderCount } = this.props;
    return (
      <div className={'rootChild'}>
        {showMessageAlert && <MessageAlert />}
        {this.state.loggedin ?
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/newcampaign" component={NewCampaign} />
            <Route path="/adminPanel" component={AdminPanel} />
          </Switch>
          : <Login />
        }
        {loaderCount > 0 && <Loader />}
      </div>
    );
  }
}


const mapStateToProps = state => ({
  showMessageAlert: state.messageAlert.get('visible'),
  isLoggedin: state.login.get('loggedIn'),
  loaderCount: loaderCountSelector(state),
});

export default withRouter(connect(mapStateToProps)(App));
