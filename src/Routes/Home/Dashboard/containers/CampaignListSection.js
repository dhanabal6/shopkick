import classNames from 'classnames';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Table from 'components/Table/Table';
import styles from '../Dashboard.css';

class CampaignListSection extends Component {
  static propTypes = {

  };
  static defaultProps = {

  };

  getAverageUnits = () => {
    const { campaignsInfo } = this.props;
    const sum = campaignsInfo && campaignsInfo.reduce((acc, item) => item.get('percent_served') + acc, 0);
    return (sum / campaignsInfo.size);
  }

  getAverageSpend = () => {
    const { campaignsInfo } = this.props;
    const sum = campaignsInfo && campaignsInfo.reduce((acc, item) => parseInt(item.get('spend_to_date')) + acc, 0);
    return (sum / campaignsInfo.size);
  }

  render() {
    const { campaignsInfo } = this.props;
    if (campaignsInfo.size === 0) { 
      return <div style={{margin:'20px 50px'}} > No Data </div> 
    }
    return (
      <div className={styles.dashboardBodySection}>
        <div className={classNames(styles.campaignUnitSectionWrapper, 'clearfix')}>
          <div className={classNames(styles.campaignUnitSection, 'fLeft')}>
            <span className={styles.campaignUnit}>
              {campaignsInfo.size}
            </span>
            <span className={styles.campaignUnitName}>
              Active Campaigns
            </span>
          </div>
          <div className={classNames(styles.campaignUnitSection, 'fLeft')}>
            <span className={styles.campaignUnit}>
              {this.getAverageUnits().toFixed(2)} {'%'}
            </span>
            <span className={styles.campaignUnitName}>
              Average % Units served
            </span>
          </div>
          <div className={classNames(styles.campaignUnitSection, 'fLeft')}>
            <span className={styles.campaignUnit}>
              {campaignsInfo.get(0).get('currency')} {this.getAverageSpend()}
            </span>
            <span className={styles.campaignUnitName}>
              Spend to Date
            </span>
          </div>
        </div>
        <Table data={campaignsInfo} />
      </div>
    );
  }
}

export default CampaignListSection;
