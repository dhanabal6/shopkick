import React, { Component } from 'react';
import classNames from 'classnames';
import moment from 'moment';
import Table from '../../../../components/Table/Table';
import styles from '../Dashboard.css';
import GanttChart from '../../../../components/GanttChart/GanttChart';

class ScheduleSection extends Component {
  constructor() {
    super();
    this.state = {
      campaign_list: [],
    };
  }
  render() {
    const {campaignsMetrics, handleYearChange, startDate, endDate} = this.props; 
    if (campaignsMetrics.size === 0) { 
        return <div style={{margin:'20px 50px'}}> No Data </div> 
    }
    var data = [];
    campaignsMetrics && campaignsMetrics.map((item) => {
        let gRow = {};
        gRow['name'] = item.get('name');
        gRow['start_date'] = item.get('start_date');
        gRow['end_date'] = item.get('end_date');
        gRow['promotions'] = item.get('promotions').toJS().length;
        gRow['spend_to_date'] = item.get('spend_to_date');
        gRow['units_served'] = item.get('units_served');
        gRow['units_contracted'] = item.get('units_contracted');
        gRow['percent_served'] = item.get('percent_served');
        gRow['impressions'] = item.get('impressions');
        gRow['is_parent'] = true;
        gRow['is_row_open'] = false;
        gRow['campaign_id'] = item.get('campaign_id');
        data.push(gRow);
        item.get('promotions') && item.get('promotions').map((child_item) => {
            let gRow = {};
            gRow['name'] = child_item.get('name');
            gRow['start_date'] = child_item.get('start_date');
            gRow['end_date'] = child_item.get('end_date');
            gRow['promotions'] = item.get('promotions').toJS().length;
            gRow['spend_to_date'] = child_item.get('spend_to_date');
            gRow['units_served'] = child_item.get('units_served');
            gRow['units_contracted'] = child_item.get('units_contracted');
            gRow['percent_served'] = child_item.get('percent_served');
            gRow['impressions'] = child_item.get('impressions');
            gRow['is_parent'] = false;
            gRow['is_row_open'] = false;
            gRow['campaign_id'] = item.get('campaign_id');
            data.push(gRow);
        });
    });
    return (
        <div className={styles.dashboardBodySection}>
            <GanttChart 
            campaignsMetrics={data} 
            handleYearChange={handleYearChange}
            startDate={startDate}
            endDate={endDate}/>
        </div>
    );
}
    
}

export default ScheduleSection;
