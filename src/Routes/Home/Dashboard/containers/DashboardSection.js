import Chart from 'chart.js';
import classNames from 'classnames';
import React, { Component } from 'react';
import DataMap from 'react-datamaps';
import HorizontalBarChart from '../../../../components/HorizontalBarChart/HorizontalBarChart';
import MapWithAMarker from '../../../../components/Map/Map';
import ProgressBar from '../../../../components/Progressbar/ProgressBar';
import TabGroup from '../../../../components/TabGroup/TabGroup';
import styles from '../Dashboard.css';


class dashboardSection extends Component {
  constructor() {
    super();
    this.state = {
      isDMA: false,
      activeTab: 'Impressions',
      age_range: {
        '18-24': {
          men: 0,
          women: 0,
          value: 0,
        },
        '25-34': {
          men: 0,
          women: 0,
          value: 0,
        },
        '35-44': {
          men: 0,
          women: 0,
          value: 0,
        },
        '45-54': {
          men: 0,
          women: 0,
          value: 0,
        },
      },
      women: 0,
      men: 0,
      hours: {
        '8-11 AM': 0,
        '11AM - 2PM': 0,
        '2-5 PM': 0,
        '5-8 PM': 0,
        '> 8PM': 0,
      },
      TotalHourValue: 0,
      activeTabState: 'State',
    };

    this.tabSelect = this.tabSelect.bind(this);
  }

  tabSelect(item) {
    if (item.name === 'State') {
      this.setState({
        isDMA: false,
        activeTabState: 'State',
      });
    } else if (item.name === 'DMA Region') {
      this.setState({
        isDMA: true,
        activeTabState: 'DMA Region',
      });
    } else {
      this.props.metricsTabSelect(item.name);

      this.setState({
        activeTab: item.name,
      });
    }
  }

  componentWillMount() {
    this.props.metricsTabSelect('Impressions');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.metricsInfo !== nextProps.metricsInfo) {
      const arr = [];
      for (let i = 0; i < 31; i++) {
        arr.push(i);
      }
      if (nextProps.metricsInfo && nextProps.metricsInfo.size > 0 && nextProps.metricsInfo.toJS()) {
        const points = nextProps.metricsInfo.toJS() || [];
        const ctx = document.getElementById('metricsChart');
        const xAxisPoints = points.map(item => new Date(item.time).getDate());
        const yAxisPoints = points.map(item => item.value);
        new Chart(ctx, {
          type: 'line',
          data: {
            datasets: [{
              pointColor: 'rgba(0,0,0,1)',
              pointStrokeColor: '#000',
              pointHighlightFill: '#000',
              pointHighlightStroke: 'rgba(82,185,159,1)',
              backgroundColor: 'rgba(255,255,255,0)',
              borderColor: '#51A5DE',
              lineTension: 0,
              data: yAxisPoints,
            }],
            labels: xAxisPoints,
          },
          responsive: false,
          bezierCurve: false,
          scaleShowVerticalLines: false,
          options: {
            scales: {
              yAxes: [{
                gridLines: {
                  color: '#E7EBEF',
                  drawBorder: false,
                },
                ticks: {
                  beginAtZero: true,
                  fontSize: 12,
                  fontColor: '#50505A',
                  fontFamily: 'Avenir-Roman',
                  maxTicksLimit: 5,
                  padding: 10,
                },
              }],
              xAxes: [{
                gridLines: {
                  display: false,
                  drawBorder: false,
                },
                ticks: {
                  beginAtZero: true,
                  fontSize: 12,
                  fontColor: '#50505A',
                  fontFamily: 'Avenir-Roman',
                  padding: 10,
                },
              }],
            },
            legend: {
              display: false,
              position: 'bottom',
            },
          },
        });
      }
    }
    if (this.props.age_gender !== nextProps.age_gender) {
      let women = 0,
        men = 0,
        v1 = [0, 0],
        v2 = [0, 0],
        v3 = [0, 0],
        v4 = [0, 0],
        TotalAgeRange = 0;
      nextProps.age_gender.length > 0 && nextProps.age_gender.forEach((item) => {
        item.tags[0].value === 'woman' ? women += item.value : men += item.value;
        if (item.tags[1].value === '18-24') {
          item.tags[0].value === 'woman' ? v1[0] += item.value : v1[1] += item.value;
        } else if (item.tags[1].value === '25-34') {
          item.tags[0].value === 'woman' ? v2[0] += item.value : v2[1] += item.value;
        } else if (item.tags[1].value === '35-44') {
          item.tags[0].value === 'woman' ? v3[0] += item.value : v3[1] += item.value;
        } else {
          item.tags[0].value === 'woman' ? v4[0] += item.value : v4[1] += item.value;
        }
        TotalAgeRange += item.value;
      });
      this.setState({
        women: parseInt(women),
        men: parseInt(men),
        age_range: {
          '18-24': {
            women: parseInt(v1[0]),
            men: parseInt(v1[1]),
            value: parseFloat(((v1[0] + v1[1]) / TotalAgeRange) * 100),
          },
          '25-34': {
            women: parseInt(v2[0]),
            men: parseInt(v2[1]),
            value: parseFloat(((v2[0] + v2[1]) / TotalAgeRange) * 100),
          },
          '35-44': {
            women: parseInt(v3[0]),
            men: parseInt(v3[1]),
            value: parseFloat(((v3[0] + v3[1]) / TotalAgeRange) * 100),
          },
          '45-54': {
            women: parseInt(v4[0]),
            men: parseInt(v4[1]),
            value: parseFloat(((v4[0] + v4[1]) / TotalAgeRange)),
          },
        },
      })
      ;
    }
    if (this.props.hour !== nextProps.hour) {
      const value = [0, 0, 0, 0, 0];
      nextProps.hour.length > 0 && nextProps.hour.forEach((item) => {
        item.tags[1].value === '8-11 AM' ? value[0] += item.value :
          item.tags[1].value === '11-2 PM' ? value[1] += item.value :
            item.tags[1].value === '2-5 PM' ? value[2] += item.value :
              item.tags[1].value === '5-8 PM' ? value[3] += item.value : value[4] += item.value;
      });
      this.setState({
        hours: {
          '8-11 AM': value[0],
          '11-2 PM': value[1],
          '2-5 PM': value[2],
          '5-8 PM': value[3],
          '> 8PM': value[4],
        },
        TotalHourValue: value[0] + value[1] + value[2] + value[3] + value[4],
      });
    }
  }


  render() {
    const { location, hour } = this.props;
    const hoursChart = [];
    const ages = ['18-24', '25-34', '35-44', '45-54'];
    const hours = ['8-11 AM', '12 AM - 2 PM', '2-5 PM', '5-8 PM', '>8 PM'];
    hour !== false && hour.forEach((item, i) => {
      let value = item.value / 100;
      value = (value > 100 ? (value - 100) : value);
      hoursChart.push(
        <ProgressBar
          key={i}
          completed={value}
          Xrange={item.tags[1].type === 'hour-range' && item.tags[1].value}
          className={styles.hourChartBar}
        />);
    });
    const data = [];
    location !== false && location.map((item) => {
      data[item.tags[0].val] = { fills: 'win' };
    });
    return (
      <div className={styles.dashboardBodySection}>
        <div className={styles.metricsSection}>
          <h4 className={classNames('fLeft', styles.infoFilterSectionTitle)}>
            Metrics</h4>
          <TabGroup
            items={
              [
                {
                  name: 'Impressions',
                  preIcon: '',
                  preIconClass: '',
                  postIcon: '',
                  postIconClass: '',
                },
                {
                  name: 'Engagements',
                  preIcon: '',
                  preIconClass: '',
                  postIcon: '',
                  postIconClass: '',
                },
                {
                  name: 'Amount Spent',
                  preIcon: '',
                  preIconClass: '',
                  postIcon: '',
                  postIconClass: '',
                },
              ]}
            activeTab={this.state.activeTab}
            className={classNames('fRight', styles.metricsTab)}
            onSelect={this.tabSelect}
          />
          <div className={'clearall'} />
          <div
            className="chart-container"
            style={{
              position: 'relative',
              height: `${90}%`,
              width: `${90}%`,
              marginTop: `${30}px`,
            }}
          >
            <canvas
              id="metricsChart"
              style={{ height: `${75}%`, width: `${90}%` }}
            />
          </div>
        </div>
        <div className={classNames('clearfix', styles.metricsAgeWrapper)}>
          <div className={classNames('fLeft', styles.metricsAge)}>
            <h4 className={classNames('fLeft', styles.infoFilterSectionTitle)}>
              Age &amp;
              Gender</h4>
            <ul className={classNames('fRight', styles.metricsAgeInfo)}>
              <li>
                <span className={styles.metricsInfoWomen} />  &nbsp;
                Women {this.state.women !== 0 && (this.state.women / (this.state.women + this.state.men) * 100).toFixed(4)}%
                ({this.state.women !== 0 && this.state.women})
              </li>
              <li>
                <span className={styles.metricsInfoMen} />  &nbsp;
                Men {this.state.men !== 0 && (this.state.men / (this.state.women + this.state.men) * 100).toFixed(4)}%
                ({this.state.men !== 0 && this.state.men})
              </li>
            </ul>
            <div className={styles.ageChartWrapper}>
              <HorizontalBarChart
                ages={ages}
                data={this.state.age_range}
              />

            </div>
            <table className={styles.percent_tbl}>
              <tbody>
                <tr>
                  <td>0%</td>
                  <td>20%</td>
                  <td>40%</td>
                  <td>60%</td>
                  <td>80%</td>
                  <td>100%</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={classNames('fLeft', styles.metricsAge)}>
            <h4 className={styles.infoFilterSectionTitle}>Hour of the day</h4>
            <div className={styles.hourChartWrapper}>
              {hoursChart}
              {/* <HorizontalBarChart
               hours={hours}
               data={this.state.hours}
               TotalHourValue={this.state.TotalHourValue}/>
               */}
            </div>
            <table className={styles.percent_tbl}>
              <tbody>
                <tr>
                  <td>0%</td>
                  <td>20%</td>
                  <td>40%</td>
                  <td>60%</td>
                  <td>80%</td>
                  <td>100%</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className={styles.locationWrapper}>
          <h4 className={classNames('fLeft', styles.infoFilterSectionTitle)}>
            Location</h4>
          <TabGroup
            items={
              [
                { name: 'State', icon: '' },
                { name: 'DMA Region', icon: '' },
              ]}
            activeTab={this.state.activeTabState}
            onSelect={this.tabSelect}
            className={classNames('fRight', styles.metricsTab)}
          />
          {!this.state.isDMA ?
            <MapWithAMarker
              containerElement={<div className={styles.map} />}
              mapElement={<div style={{ height: '100%', width: '100%' }} />}
              location={location}
            /> :
            <div style={{ marginTop: 35, height: '100%' }} >
              <DataMap
                scope="usa"
                fills={{
                  defaultFill: '#0fa0fa',
                  win: '#abdda4',
                }}
                data={{
                  FL: { fillKey: 'win' },
                  UT: { fillKey: 'win' },
                  AZ: { fillKey: 'win' },
                  CA: { fillKey: 'win' },
                }}
              />
            </div>
          }
          <div className={'clearall'} />
        </div>
      </div>
    );
  }
}


export default dashboardSection;
