import classNames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import TabGroup from '../../../components/TabGroup/TabGroup';
import CampaignHeader from '../../../containers/CampaignHeader/index';
import InfoAssistant from '../../../containers/InfoAssistant/index';
import CampaignListSection from './containers/CampaignListSection';
import DashboardSection from './containers/DashboardSection';
import ScheduleSection from './containers/ScheduleSection';
import {
  getAgeGender,
  getCampaignDetails,
  getCampaignMetrics,
  getFilterDetails,
  getHour,
  getLocation,
  getMetrics,
  getUserDetails,
  infoAssistant,
} from './Dashboard.actions';
import styles from './Dashboard.css';
import { dashboardSelector } from './Dashboard.selectors';

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      activeTab: 'Dashboard',
      infoAssistActive: false,
      startDate: moment().startOf('year').format('X'),
      endDate: moment().endOf('year').format('X'),
    };
    this.tabSelect = this.tabSelect.bind(this);
    this.handleApplyFilter = this.handleApplyFilter.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
  }

  componentWillMount() {
    const userId = localStorage.getItem('userId');
    const { dispatch } = this.props;
    dispatch(getUserDetails(userId));
    dispatch(getFilterDetails(userId));
    dispatch(getCampaignDetails(this.state.startDate, this.state.endDate));
    dispatch(getAgeGender('impressions', this.state.startDate, this.state.endDate));
    dispatch(getHour('impressions', this.state.startDate, this.state.endDate));
    dispatch(getLocation('impressions', this.state.startDate, this.state.endDate));
    dispatch(getCampaignMetrics(this.state.startDate, this.state.endDate));
  }

  handleApplyFilter(startDate, endDate) {
    const userId = localStorage.getItem('userId');
    const { dispatch } = this.props;
    dispatch(infoAssistant(startDate, endDate, userId));
    dispatch(getFilterDetails(userId));
  }

  handleYearChange(tsStart, tsEnd) {
    this.setState({ startDate: tsStart, endDate: tsEnd });
  }

  tabSelect(item) {
    this.setState({
      activeTab: item.name,
    });
  }

  render() {
    const { userInfo, filtersInfo, campaignsInfo, age_gender, hour, location, metricsInfo, dispatch } = this.props;
    return (
      <div className={styles.wrapper}>
        <CampaignHeader
          userInfo={userInfo.size > 0 && userInfo.toJS()}
        />
        <div className={styles.cmBodyWrapper}>
          <InfoAssistant
            filtersInfo={filtersInfo.toJS()}
            userInfo={userInfo.toJS()}
            dispatch={dispatch}
            updateState={(startDate, endDate) => this.setState({
              startDate,
              endDate,
            })}
            handleApplyFilter={(startDate, endDate) => this.handleApplyFilter(startDate, endDate)}
            infoAssistActive={val => this.setState({ infoAssistActive: val })}
          />
          {!this.state.infoAssistActive &&
          <div className={styles.mainSectionWrapper}>
            <div className={classNames('clearfix', styles.mainSectionHeader)}>
              <TabGroup
                items={
                  [
                    {
                      name: 'Dashboard',
                      preIcon: 'dashboard',
                      preIconClass: styles.dashboardIcon,
                      postIcon: '',
                      postIconClass: '',
                    },
                    {
                      name: 'Campaigns',
                      preIcon: 'view_headline',
                      preIconClass: styles.dashboardIcon,
                      postIcon: '',
                      postIconClass: '',
                    },
                    {
                      name: 'Schedule',
                      preIcon: 'today',
                      preIconClass: styles.dashboardIcon,
                      postIcon: '',
                      postIconClass: '',
                    },
                  ]}
                activeTab={this.state.activeTab}
                onSelect={this.tabSelect}
              />
            </div>
            {this.state.activeTab === 'Dashboard' &&
            <DashboardSection
              age_gender={age_gender.size > 0 && age_gender.toJS()}
              hour={hour.size > 0 && hour.toJS()}
              location={location.size > 0 && location.toJS()}
              metricsInfo={metricsInfo}
              metricsTabSelect={(tab) => {
                this.props.dispatch(getMetrics(localStorage.getItem('userId'), tab.toLowerCase(), this.state.startDate, this.state.endDate)) &&
                this.props.dispatch(getAgeGender(tab.toLowerCase(), this.state.startDate, this.state.endDate)) &&
                this.props.dispatch(getHour(tab.toLowerCase(), this.state.startDate, this.state.endDate)) &&
                this.props.dispatch(getLocation(tab.toLowerCase(), this.state.startDate, this.state.endDate));
              }}
            />}
            {this.state.activeTab === 'Campaigns' &&
            <CampaignListSection campaignsInfo={campaignsInfo} />}
            {this.state.activeTab === 'Schedule' &&
            <ScheduleSection campaignsMetrics={campaignsInfo}
                             handleYearChange={(tsStart, tsEnd) => this.handleYearChange(tsStart, tsEnd)}
                             startDate={this.state.startDate}
                             endDate={this.state.endDate} />}
          </div>}
        </div>
      </div>
    );
  }
}


export const mapStateToProps = state => dashboardSelector(state);

export default connect(mapStateToProps)(Dashboard);
