export const dashboardSelector = state => ({
  userInfo: state.dashboard.get('userInfo'),
  filtersInfo: state.dashboard.get('filtersInfo'),
  campaignsInfo: state.dashboard.get('campaignsInfo'),
  metricsInfo: state.dashboard.get('metricsInfo'),
  age_gender: state.dashboard.get('age_gender'),
  hour: state.dashboard.get('hour'),
  location: state.dashboard.get('location'),
  campaignsMetrics: state.dashboard.get('campaignsMetrics'),
});
