import {
  ACTION_GET_AGE_GENDER,
  ACTION_GET_CAMPAIGN_METRICS,
  ACTION_GET_CAMPAIGNS_DETAILS,
  ACTION_GET_FILTER_DETAILS,
  ACTION_GET_HOUR,
  ACTION_GET_LOCATION,
  ACTION_GET_METRICS,
  ACTION_GET_USER_DETAILS,
  ACTION_LOGOUT,
  ACTION_POST_INFOASSISTANT,
  ACTION_SET_AGE_GENDER,
  ACTION_SET_HOUR,
  ACTION_SET_LOCATION,
  ACTION_STORE_CAMPAIGN_METRICS,
  ACTION_STORE_CAMPAIGNS_INFO,
  ACTION_STORE_FILTER_INFO,
  ACTION_STORE_METRICS,
  ACTION_STORE_USER_PROFILE_INFO,
} from './Dashboard.constants';

export function getUserDetails(userId) {
  return {
    type: ACTION_GET_USER_DETAILS,
    payload: { userId },
  };
}

export function getFilterDetails(userId) {
  return {
    type: ACTION_GET_FILTER_DETAILS,
    payload: { userId },
  };
}

export function getCampaignDetails(startDate, endDate) {
  return {
    type: ACTION_GET_CAMPAIGNS_DETAILS,
    payload: { startDate, endDate },
  };
}

export function storeUserProfileInfo(response) {
  return {
    type: ACTION_STORE_USER_PROFILE_INFO,
    payload: { response },
  };
}


export function storeFilterInfo(response) {
  return {
    type: ACTION_STORE_FILTER_INFO,
    payload: { response },
  };
}


export function storeAllCampaignsInfo(response) {
  return {
    type: ACTION_STORE_CAMPAIGNS_INFO,
    payload: { response },
  };
}


export function infoAssistant(startDate, endDate, userId) {
  return {
    type: ACTION_POST_INFOASSISTANT,
    payload: {
      startDate,
      endDate,
      userId,
    },
  };
}

export function setAgeGender(response) {
  return {
    type: ACTION_SET_AGE_GENDER,
    payload: { response },
  };
}

export function getAgeGender(type, startDate, endDate) {
  return {
    type: ACTION_GET_AGE_GENDER,
    payload: { type, startDate, endDate },
  };
}

export function getHour(type, startDate, endDate) {
  return {
    type: ACTION_GET_HOUR,
    payload: { type, startDate, endDate },
  };
}

export function setHour(response) {
  return {
    type: ACTION_SET_HOUR,
    payload: { response },
  };
}

export function getLocation(type, startDate, endDate) {
  return {
    type: ACTION_GET_LOCATION,
    payload: { type, startDate, endDate },
  };
}

export function setLocation(response) {
  return {
    type: ACTION_SET_LOCATION,
    payload: { response },
  };
}

export function getMetrics(userId, type, startDate, endDate) {
  return {
    type: ACTION_GET_METRICS,
    payload: { userId, type, startDate, endDate },
  };
}

export function storeMetrics(response) {
  return {
    type: ACTION_STORE_METRICS,
    payload: { response },
  };
}

export function logout(userId) {
  return {
    type: ACTION_LOGOUT,
    payload: userId,
  };
}

export function getCampaignMetrics(tsStart, tsEnd) {
  return {
    type: ACTION_GET_CAMPAIGN_METRICS,
    payload: { tsStart, tsEnd },
  };
}

export function storeCampaignMetrics(response) {
  return {
    type: ACTION_STORE_CAMPAIGN_METRICS,
    payload: { response },
  };
}
