import { fromJS } from 'immutable';
import { createReducerFromObject } from '../../../utils/reducerUtils';
import {
  ACTION_SET_AGE_GENDER,
  ACTION_SET_HOUR,
  ACTION_SET_LOCATION,
  ACTION_STORE_CAMPAIGN_METRICS,
  ACTION_STORE_CAMPAIGNS_INFO,
  ACTION_STORE_FILTER_INFO,
  ACTION_STORE_METRICS,
  ACTION_STORE_USER_PROFILE_INFO,
} from './Dashboard.constants';

export const initialState = fromJS({
  userInfo: {},
  filtersInfo: {},
  campaignsInfo: [],
  metricsInfo: [],
  age_gender: [],
  hour: [],
  location: [],
  campaignsMetrics: [],
});

const reducerFunctions = {
  [ACTION_STORE_USER_PROFILE_INFO]: (state, payload) => state.set('userInfo', fromJS(payload.response)),
  [ACTION_STORE_FILTER_INFO]: (state, payload) => state.set('filtersInfo', fromJS(payload.response)),
  [ACTION_STORE_CAMPAIGNS_INFO]: (state, payload) => state.set('campaignsInfo', fromJS(payload.response)),
  [ACTION_SET_AGE_GENDER]: (state, payload) => state.set('age_gender', fromJS(payload.response)),
  [ACTION_SET_HOUR]: (state, payload) => state.set('hour', fromJS(payload.response)),
  [ACTION_SET_LOCATION]: (state, payload) => state.set('location', fromJS(payload.response)),
  [ACTION_STORE_METRICS]: (state, payload) => state.set('metricsInfo', fromJS(payload.response)),
  [ACTION_STORE_CAMPAIGN_METRICS]: (state, payload) => state.set('campaignsMetrics', fromJS(payload.response)),
};

const dashboardReducer = createReducerFromObject(reducerFunctions, initialState);
export default dashboardReducer;
