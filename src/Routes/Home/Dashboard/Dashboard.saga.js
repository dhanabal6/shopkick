import { replace } from 'react-router-redux';
import { call, fork, put, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../index';

import request from '../../../utils/request';
import * as actions from './Dashboard.actions';
import * as constants from './Dashboard.constants';
import fallback from './fallback';


function showAlert(error) {
  if (!window.navigator.onLine) {
    store.dispatch(showMessageAlert({
      message: 'Please check your internet connection',
      visible: true,
      color: '#e53e3d',
    }));
  } else {
    store.dispatch(showMessageAlert({
      message: error.message,
      visible: true,
      color: '#e53e3d',
    }));
  }
}

export function* getUserDetails({ payload }) {
  try {
    yield call(request, constants.URL_USER_DETAIL + payload.userId, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.storeUserProfileInfo(response));
        },
        onError() {
          store.dispatch(showMessageAlert({
            message: 'Error fetching user details!',
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* filterDetails({ payload }) {
  const url = `v1/user/${payload.userId}/dashboard_profile`;
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.storeFilterInfo(response));
        },
        onError() {
          store.dispatch(actions.storeFilterInfo(fallback.filterDetails));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    yield store.dispatch(actions.storeFilterInfo(fallback.filterDetails));
    showAlert(error);
  }
}

export function* campaignDetails({ payload }) {
  let url;
  if (payload.searchText) {
    url = (`/v1/campaigns/${payload.searchText}/${payload.userId}`);
  } else {
    url = `v1/campaign_metrics?from_ts=${payload.startDate}&to_ts=${payload.endDate}`;
  }
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.storeAllCampaignsInfo(response));
        },
        onError() {
          store.dispatch(actions.storeAllCampaignsInfo(fallback.campaignDetails));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    yield store.dispatch(actions.storeAllCampaignsInfo(fallback.campaignDetails));
    showAlert(error);
  }
}

export function* hour({ payload }) {
  const url = `/v1/campaign_metrics/${(payload.type === 'amount spent') ? 'amount_spent' : payload.type}/day_hours?from_ts=${payload.startDate}&to_ts=${payload.endDate}`;
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.setHour(response));
        },
        onError() {
          store.dispatch(actions.setHour(fallback.hour));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* infoAssistant({ payload }) {
  try {
    yield call(request, `${constants.URL_INFOASSISTANT + payload.userId}/dashboard_profile/update`, {
        method: 'POST',
        body: {
          from_date: payload.startDate,
          to_date: payload.endDate,
          locations_filters: [],
        },
      },
      {
        onSuccess() {
          store.dispatch(showMessageAlert({
            message: 'User preferences saved successfully',
            visible: true,
            color: '#63e56a',
          }));
        },
        onError(response) {

        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* ageGender({ payload }) {
  const url = `/v1/campaign_metrics/${(payload.type === 'amount spent') ? 'amount_spent' : payload.type}/age_gender?from_ts=${payload.startDate}&to_ts=${payload.endDate}`;
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.setAgeGender(response));
        },
        onError() {
          store.dispatch(actions.setAgeGender(fallback.ageGender));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* location({ payload }) {
  const url = `/v1/campaign_metrics/${(payload.type === 'amount spent') ? 'amount_spent' : payload.type}/location?from_ts=${payload.startDate}&to_ts=${payload.endDate}`;
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.setLocation(response));
        },
        onError() {
          store.dispatch(actions.setLocation(fallback.location));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* metricsFn({ payload }) {
  const url = `/v1/campaign_metrics/${(payload.type === 'amount spent') ? 'amount_spent' : payload.type}?from_ts=${payload.startDate}&to_ts=${payload.endDate}`;
  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess() {
          store.dispatch(actions.storeMetrics(fallback.metrics));
        },
        onError() {
          store.dispatch(actions.storeMetrics(fallback.metrics));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
}

export function* campaignMetricsList({ payload }) {
  const url = `v1/campaign_metrics?from_ts=${payload.tsStart}&to_ts=${payload.tsEnd}`;

  try {
    yield call(request, url, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(actions.storeCampaignMetrics(response));
        },
        onError() {
          store.dispatch(decrementLoaderCount());
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    yield store.dispatch(actions.storeCampaignMetrics(fallback.campaignMetrics));
    showAlert(error);
  }
}


export function* logout({ payload }) {
  try {
    yield call(request, constants.URL_LOGOUT, {
        method: 'POST',
        body: {
          user_id: payload,
        },
      },
      {
        onSuccess() {
          const isRemember = localStorage.getItem('shopkick-login-remember');
          localStorage.clear();
          localStorage.setItem('shopkick-login-remember', isRemember);
          store.dispatch(showMessageAlert({
            message: 'Logged out succesfully!',
            visible: true,
            color: '#63e56a',
          }));
        },
        onError(error) {
          store.dispatch(showMessageAlert({
            message: error.message,
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    const isRemember = localStorage.getItem('shopkick-login-remember');
    localStorage.clear();
    localStorage.setItem('shopkick-login-remember', isRemember);
    yield store.dispatch(decrementLoaderCount());
    showAlert(error);
  }
  yield put(replace('/'));
}

export default function* dashboardSagas() {
  yield [
    fork(takeLatest, constants.ACTION_GET_USER_DETAILS, getUserDetails),
    fork(takeLatest, constants.ACTION_GET_FILTER_DETAILS, filterDetails),
    fork(takeLatest, constants.ACTION_GET_CAMPAIGNS_DETAILS, campaignDetails),
    fork(takeLatest, constants.ACTION_GET_HOUR, hour),
    fork(takeLatest, constants.ACTION_POST_INFOASSISTANT, infoAssistant),
    fork(takeLatest, constants.ACTION_GET_AGE_GENDER, ageGender),
    fork(takeLatest, constants.ACTION_GET_LOCATION, location),
    fork(takeLatest, constants.ACTION_GET_METRICS, metricsFn),
    fork(takeLatest, constants.ACTION_GET_CAMPAIGN_METRICS, campaignMetricsList),
    fork(takeLatest, constants.ACTION_LOGOUT, logout),
  ];
}
