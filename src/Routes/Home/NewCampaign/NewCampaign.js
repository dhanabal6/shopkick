import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import CampaignHeader from '../../../containers/CampaignHeader/index';
import { getUserDetails } from '../Dashboard/Dashboard.actions';
import CampaignNavigator from './containers/CampaignNavigator/CampaignNavigator';
import NewCampaignHeader from './containers/NewCampaignHeader/NewCampaignHeader';
import NewCampaignMainBody from './containers/NewCampaignMainBody/NewCampaignMainBody';
import NewCampaignMetrics from './containers/NewCampaignMetrics/NewCampaignMetrics';
import {
  getAllChain,
  getAllDMA,
  getAllLocation,
} from './NewCampaign.actions';
import styles from './NewCampaign.css';
import { newCampaignSelector } from './NewCampaign.selectors';

class NewCampaign extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
  };
  static defaultProps = {
    dispatch: f => f,
  };

  constructor() {
    super();
    this.state = {
      activeNavLink: '',
      campaignName: '',
      campaignDetail: '',
      campaignObjective: '',
      budgetStartDate: '',
      budgetEndDate: '',
      budgetValue: '',
    };
    this.updateNavLinks = this.updateNavLinks.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    const userId = localStorage.getItem('userId');
    dispatch(getUserDetails(userId));
    dispatch(getAllChain());
    dispatch(getAllDMA());
    dispatch(getAllLocation());
  }

  updateNavLinks(count) {
    switch (count) {
      case 3:
        this.setState({
          activeNavLink: 'Campaign',
        });
        break;
      case 6:
        this.setState({
          activeNavLink: 'Promotion Set',
        });
        break;
      case 9:
        this.setState({
          activeNavLink: 'Promotion',
        });
    }
  }

  render() {
    const { userInfo, dispatch, newCampaignDetails, promotionSetsInfo } = this.props;
    return (
      <div className={'bodyWrapper'}>
        <CampaignHeader
          userInfo={userInfo.size > 0 && userInfo.toJS()}
        />
        <div className={classNames('clearfix', 'cmBodyWrapper')}>
          <CampaignNavigator
            getActiveNavLink={(link) => {
              this.setState({ activeNavLink: link });
            }}
            getDetails={this.state}
            dispatch={dispatch}
            updateActiveLink={this.state.activeNavLink}
          />
          <div className={classNames(styles.rightSectionWrapper, 'fLeft')}>
            <NewCampaignHeader
              activeNavLink={this.state.activeNavLink}
              getDetails={this.state}
              dispatch={dispatch}
              updateNavLinks={count => this.updateNavLinks(count)}
            />
            <div className={classNames(styles.bodyWrapper, 'clearfix')}>
              <NewCampaignMainBody
                dispatch={dispatch}
                userId={userInfo.get('id')}
                activeNavLink={this.state.activeNavLink}
                sendOverviewDetails={(campaignName, campaignDetail) =>
                  this.setState({
                    campaignName,
                    campaignDetail,
                  })
                }
                sendObjectiveDetails={(campaignObjective) => {
                  this.setState({
                    campaignObjective,
                  });
                }}
                sendBudgetDates={(budgetValue, budgetStartDate, budgetEndDate) => {
                  this.setState({
                    budgetValue,
                    budgetStartDate,
                    budgetEndDate,
                  });
                }
                }
                newCampaignDetails={newCampaignDetails}
                promotionSetsInfo={promotionSetsInfo}
              />
              <NewCampaignMetrics
                startDate={this.state.budgetStartDate}
                budgetValue={this.state.budgetValue}
                endDate={this.state.budgetEndDate}
              />
            </div>
          </div>
        </div>
      </div>
    )
      ;
  }
}

export const mapStateToProps = state => newCampaignSelector(state);

export default connect(mapStateToProps)(NewCampaign);
