import {
  ACTION_GET_ALL_PROMOTIONSETS,
  ACTION_SET_STATE_LOCATIONS,
  ACTION_SET_CITY_LOCATIONS,
  ACTION_GET_CAMPAIGN_BY_ID,
  ACTION_GET_CHAINS,
  ACTION_GET_DMA,
  ACTION_GET_LOCATIONS,
  ACTION_GET_CITY_LOCATIONS,
  ACTION_SAVE_BUDGET_SCHEDULE,
  ACTION_SAVE_CAMPAIGN_OBJECTIVE,
  ACTION_SAVE_CAMPAIGNOVERVIEW,
  ACTION_SAVE_LOCATION,
  ACTION_SAVE_PROMOTIONSET,
  ACTION_SET_CHAINS,
  ACTION_SET_DMA,
  ACTION_STORE_NEWCAMPAIGN,
  ACTION_STORE_PROMOTIONSETS,
} from './NewCampaign.constants';


export function saveNewCampaignOverviewDetails(name, details) {
  return {
    type: ACTION_SAVE_CAMPAIGNOVERVIEW,
    payload: { name, details },
  };
}

export function saveNewCampaignObjective(type, value) {
  return {
    type: ACTION_SAVE_CAMPAIGN_OBJECTIVE,
    payload: { type, value },
  };
}


export function saveBudgetDetails(budget_sched_management, budgetType, budget_amount, budget_currency, start_date, end_date) {
  let budget_type;
  switch (budgetType) {
    case 'Total Campaign' : {
      budgetType = 'campaign_total';
      break;
    }
    case 'Amount per Day' : {
      budget_type = 'amt_per_day';
      break;
    }
    case 'Amount per Week' : {
      budget_type = 'amt_per_week';
      break;
    }
    default : {
      budget_type = 'amt_per_engagement';
      break;
    }
  }
  return {
    type: ACTION_SAVE_BUDGET_SCHEDULE,
    payload: { budget_sched_management, budget_type, budget_amount, budget_currency, start_date, end_date },
  };
}

export function savePromotionSet(name, details, promo_topic, promo_item, campaign_id, locations_filters, user_profile_filters) {
  return {
    type: ACTION_SAVE_PROMOTIONSET,
    payload: { name, details, promo_topic, promo_item, campaign_id, locations_filters, user_profile_filters },
  };
}

export function getAllSavedPromotions(id) {
  return {
    type: ACTION_GET_ALL_PROMOTIONSETS,
    payload: { id },
  };
}

export function storePromotionSets() {
  return {
    type: ACTION_STORE_PROMOTIONSETS,
  };
}

export function storeNewCampaignDetails(response) {
  return {
    type: ACTION_STORE_NEWCAMPAIGN,
    payload: response,
  };
}

export function getCampaignById(id) {
  return {
    type: ACTION_GET_CAMPAIGN_BY_ID,
    payload: id,
  };
}

export function getAllLocation(value) {
  return {
    type: ACTION_GET_LOCATIONS,
    payload: value,
  };
}

export function getAllCityLocation(name) {
  return {
    type: ACTION_GET_CITY_LOCATIONS,
    payload: name,
  };
}

export function setAllStateLocation(response) {
  return {
    type: ACTION_SET_STATE_LOCATIONS,
    payload: { response },
  };
}

export function setAllCityLocation(response) {
  return {
    type: ACTION_SET_CITY_LOCATIONS,
    payload: { response },
  };
}


export function getAllChain(value) {
  return {
    type: ACTION_GET_CHAINS,
    payload: value,
  };
}

export function setAllChain(response) {
  return {
    type: ACTION_SET_CHAINS,
    payload: { response },
  };
}

export function getAllDMA(value) {
  return {
    type: ACTION_GET_DMA,
    payload: value,
  };
}

export function setAllDMA(response) {
  return {
    type: ACTION_SET_DMA,
    payload: { response },
  };
}

export function saveLocation(data) {
  return {
    type: ACTION_SAVE_LOCATION,
    payload: { data },
  };
}

