import { call, fork, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import { store } from '../../../../index';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { ACTION_GET_LOCATIONS, ACTION_GET_CITY_LOCATIONS, ACTION_GET_CHAINS, ACTION_GET_DMA, URL_LOCATIONS_STATE, URL_LOCATIONS_CITY, URL_CHAINS, URL_DMA } from '../NewCampaign.constants';
import { setAllCityLocation, setAllStateLocation, setAllChain, setAllDMA } from '../NewCampaign.actions';

export function* locationStates() {
  try {
    yield call(request, URL_LOCATIONS_STATE, {
      method: 'GET',
    },
    {
      onSuccess(response) {
        store.dispatch(setAllStateLocation(response));
      },
      onError() {
      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export function* locationCity(action) {
  try {
    yield call(request, URL_LOCATIONS_CITY + action.payload, {
      method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(setAllCityLocation(response));
        },
        onError() {
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export function* chains() {
  try {
    yield call(request, URL_CHAINS, {
      method: 'GET',
    },
    {
      onSuccess(response) {
        store.dispatch(setAllChain(response));
      },
      onError() {
      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export function* dma() {
  try {
    yield call(request, URL_DMA, {
      method: 'GET',
    },
    {
      onSuccess(response) {
        store.dispatch(setAllDMA(response));
      },
      onError() {
      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* searchSaga() {
  yield fork(takeLatest, ACTION_GET_LOCATIONS, locationStates);
  yield fork(takeLatest, ACTION_GET_CITY_LOCATIONS, locationCity);
  yield fork(takeLatest, ACTION_GET_CHAINS, chains);
  yield fork(takeLatest, ACTION_GET_DMA, dma);
}
