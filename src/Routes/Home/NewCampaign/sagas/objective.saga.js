import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { ACTION_SAVE_CAMPAIGN_OBJECTIVE } from '../NewCampaign.constants';

export function* campaignObjective(action) {
  const newCampaignId = localStorage.getItem('newCampaignId') && parseInt(localStorage.getItem('newCampaignId'));
  const URL_OBJECTIVE = `v1/campaign/${newCampaignId}/wizardcreate/objective`;
  try {
    const { type, value } = action.payload;
    yield call(request, URL_OBJECTIVE, {
        method: 'POST',
        body: {
          objective: {
            type, value,
          },
        },
      },
      {
        onSuccess() {

        },
        onError() {
          store.dispatch(showMessageAlert({
            message: 'Error saving objective! Please try again',
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    store.dispatch(showMessageAlert({
      message: 'Error saving objective! Please try again',
      visible: true,
      color: '#e53e3d',
    }));
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* objectiveSaga() {
  yield fork(takeLatest, ACTION_SAVE_CAMPAIGN_OBJECTIVE, campaignObjective);
}
