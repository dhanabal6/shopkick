import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { setSavedLocation } from '../NewCampaign.actions';
import { ACTION_GET_SAVEDLOCATIONS, URL_SAVEDLOCATIONS } from '../NewCampaign.constants';

export function* savedLocation(action) {
  try {
    yield call(request, URL_SAVEDLOCATIONS, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(setSavedLocation(response));
        },
        onError(response) {

        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* savedLocationSaga() {
  yield fork(takeLatest, ACTION_GET_SAVEDLOCATIONS, savedLocation);
}
