import { call, fork, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import { store } from '../../../../index';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { ACTION_SAVE_LOCATION, URL_SAVELOCATION } from '../NewCampaign.constants';

export function* saveLocation(action) {
  try {
    const { data } = action.payload;
    yield call(request, URL_SAVELOCATION, {
      method: 'POST',
      body: {
        data,
      },
    },
    {
      onSuccess(response) {

      },
      onError(response) {

      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* saveLocationSaga() {
  yield fork(takeLatest, ACTION_SAVE_LOCATION, saveLocation);
}
