import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { ACTION_SAVE_PROMOTIONSET, URL_SAVE_PROMOTIONSET } from '../NewCampaign.constants';

export function* promotionSet(action) {
  try {
    const { name, details, promo_topic, promo_item, campaign_id, locations_filters, user_profile_filters } = action.payload;
    yield call(request, URL_SAVE_PROMOTIONSET, {
        method: 'POST',
        body: {
          name, details, promo_topic, promo_item, campaign_id, locations_filters, user_profile_filters,
        },
      },
      {
        onSuccess() {
          store.dispatch(showMessageAlert({
            message: 'Promotion set saved',
            visible: true,
            color: '#98f07d',
          }));
        },
        onError(response) {
          store.dispatch(showMessageAlert({
            message: response.message,
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* createPromotionSetSaga() {
  yield fork(takeLatest, ACTION_SAVE_PROMOTIONSET, promotionSet);
}
