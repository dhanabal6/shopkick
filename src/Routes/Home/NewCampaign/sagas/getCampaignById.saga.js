import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { storeNewCampaignDetails } from '../NewCampaign.actions';
import { ACTION_GET_CAMPAIGN_BY_ID, URL_GET_NEWCAMPAIGN_BY_ID } from '../NewCampaign.constants';

export function* getCampaign(action) {
  try {
    const id = action.payload;
    yield call(request, URL_GET_NEWCAMPAIGN_BY_ID + id, {
      method: 'GET',
    },
    {
      onSuccess(response) {
        store.dispatch(storeNewCampaignDetails(response));
      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* getCampaignSaga() {
  yield fork(takeLatest, ACTION_GET_CAMPAIGN_BY_ID, getCampaign);
}
