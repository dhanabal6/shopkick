import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { setBrowseLocation } from '../NewCampaign.actions';
import { ACTION_GET_BROWSE_LOCATIONS, URL_BROWSELOCATIONS } from '../NewCampaign.constants';

export function* broseLocation(action) {
  try {
    yield call(request, URL_BROWSELOCATIONS, {
        method: 'GET',
      },
      {
        onSuccess(response) {
          store.dispatch(setBrowseLocation(response));
        },
        onError(response) {
          store.dispatch(showMessageAlert({
            message: 'Error fetching locations',
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    store.dispatch(showMessageAlert({
      message: 'Error fetching locations',
      visible: true,
      color: '#e53e3d',
    }));
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* broseLocationSaga() {
  yield fork(takeLatest, ACTION_GET_BROWSE_LOCATIONS, broseLocation);
}
