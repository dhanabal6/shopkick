import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { ACTION_SAVE_BUDGET_SCHEDULE } from '../NewCampaign.constants';

export function* budgetSchedule(action) {
  try {
    const { budget_sched_management, budget_type, budget_amount, budget_currency, start_date, end_date } = action.payload;
    const newCampaignId = localStorage.getItem('newCampaignId') && parseInt(localStorage.getItem('newCampaignId'));
    const URL_BUDGET = `v1/campaign/${newCampaignId}/wizardcreate/budget_schedule`;
    yield call(request, URL_BUDGET, {
        method: 'POST',
        body: {
          budget_sched_management,
          budget_type,
          budget_amount,
          budget_currency,
          start_date,
          end_date,
        },
      },
      {
        onSuccess(response) {

        },
        onError() {
          store.dispatch(showMessageAlert({
            message: 'Error saving Budget details! Please try again',
            visible: true,
            color: '#e53e3d',
          }));
        }
        ,
      }
      ,
      true,
    )
    ;
  } catch (error) {
    store.dispatch(showMessageAlert({
      message: 'Error saving Budget details! Please try again',
      visible: true,
      color: '#e53e3d',
    }));
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* objectiveSaga() {
  yield fork(takeLatest, ACTION_SAVE_BUDGET_SCHEDULE, budgetSchedule);
}
