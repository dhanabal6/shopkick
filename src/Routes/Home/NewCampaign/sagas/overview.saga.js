import { call, fork, takeLatest } from 'redux-saga/effects';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { store } from '../../../../index';
import request from '../../../../utils/request';
import { storeNewCampaignDetails } from '../NewCampaign.actions';
import { ACTION_SAVE_CAMPAIGNOVERVIEW, URL_OVERVIEW } from '../NewCampaign.constants';

export function* campaignOverview(action) {
  try {
    const { name, details } = action.payload;
    yield call(request, URL_OVERVIEW, {
        method: 'POST',
        body: {
          name, details,
        },
      },
      {
        onSuccess(response) {
          store.dispatch(storeNewCampaignDetails({ id: response.id }));
          localStorage.setItem('newCampaignId', JSON.stringify(response.id));
        },
        onError(response) {
          store.dispatch(showMessageAlert({
            message: response.message,
            visible: true,
            color: '#e53e3d',
          }));
        },
      }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* overviewSaga() {
  yield fork(takeLatest, ACTION_SAVE_CAMPAIGNOVERVIEW, campaignOverview);
}
