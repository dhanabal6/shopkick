import { call, fork, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import { store } from '../../../../index';
import { decrementLoaderCount } from '../../../../containers/Loader/Loader.actions';
import { showMessageAlert } from '../../../../containers/MessageAlert/MessageAlert.actions';
import { ACTION_GET_ALL_PROMOTIONSETS, URL_GET_PROMOTIONSETS } from '../NewCampaign.constants';
import { storePromotionSets } from '../NewCampaign.actions';

export function* getPromotionSet(action) {
  yield store.dispatch(storePromotionSets());
  try {
    const { id } = action.payload;
    yield call(request, `${URL_GET_PROMOTIONSETS}?user=${id}`, {
      method: 'GET',
    },
    {
      onSuccess(response) {

      },
      onError(response) {

      },
    }, true);
  } catch (error) {
    yield store.dispatch(decrementLoaderCount());
    if (!window.navigator.onLine) {
      store.dispatch(showMessageAlert({
        message: 'Please check your internet connection',
        visible: true,
        color: '#e53e3d',
      }));
    } else {
      store.dispatch(showMessageAlert({
        message: error.message,
        visible: true,
        color: '#e53e3d',
      }));
    }
  }
}

export default function* getPromotionSaga() {
  yield fork(takeLatest, ACTION_GET_ALL_PROMOTIONSETS, getPromotionSet);
}
