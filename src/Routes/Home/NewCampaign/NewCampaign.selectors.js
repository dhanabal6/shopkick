export const newCampaignSelector = state => ({
  userInfo: state.dashboard.get('userInfo'),
  promotionSetsInfo: state.newCampaign.get('promotionSets'),
  newCampaignDetails: state.newCampaign.get('newCampaignDetails'),
  stateLocations: state.newCampaign.get('stateLocations'),
  cityLocations: state.newCampaign.get('cityLocations'),
  chains: state.newCampaign.get('chains'),
  dma: state.newCampaign.get('dma'),
});
