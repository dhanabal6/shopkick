import { BASE_URL } from '../../../constants/urls';

export const ACTION_SAVE_CAMPAIGNOVERVIEW = 'shopkick/newCampaign/save_newCampaignOverview';
export const ACTION_SAVE_CAMPAIGN_OBJECTIVE = 'shopkick/newCampaign/save_newCampaignObjective';
export const ACTION_SAVE_BUDGET_SCHEDULE = 'shopkick/newCampaign/save_saveBudgetSchedule';
export const ACTION_SAVE_PROMOTIONSET = 'shopkick/newCampaign/save_promotionset';
export const ACTION_GET_ALL_PROMOTIONSETS = 'shopkick/newCampaign/get_promotionsets';
export const ACTION_STORE_PROMOTIONSETS = 'shopkick/newCampaign/store_promotionsets';
export const ACTION_STORE_NEWCAMPAIGN = 'shopkick/newCampaign/store_newCampaign';
export const ACTION_GET_CAMPAIGN_BY_ID = 'shopkick/newCampaign/get_newCampaignById';
export const ACTION_GET_LOCATIONS = 'shopkick/newCampaign/promotionset/getLocations';
export const ACTION_GET_CITY_LOCATIONS = 'shopkick/newCampaign/promotionset/getCityLocations';
export const ACTION_SET_STATE_LOCATIONS = 'shopkick/newCampaign/promotionset/getStateLocations';
export const ACTION_SET_CITY_LOCATIONS = 'shopkick/newCampaign/promotionset/setCityLocations';
export const ACTION_GET_CHAINS = 'shopkick/newCampaign/promotionset/getChains';
export const ACTION_SET_CHAINS = 'shopkick/newCampaign/promotionset/setChains';
export const ACTION_GET_DMA = 'shopkick/newCampaign/promotionset/getDMA';
export const ACTION_SET_DMA = 'shopkick/newCampaign/promotionset/setDMA';
export const ACTION_SAVE_LOCATION = 'shopkick/newCampaign/promotionset/saveLocation';


const newCampaignId = localStorage.getItem('newCampaignId') && parseInt(localStorage.getItem('newCampaignId'));
export const URL_OVERVIEW = `${BASE_URL}/campaign/wizardcreate/overview`;
export const URL_BUDGET = `${BASE_URL}/campaign/${newCampaignId}/wizardcreate/budget_schedule`;
export const URL_OBJECTIVE = `${BASE_URL}/campaign/${newCampaignId}/wizardcreate/objective`;
export const URL_SAVE_PROMOTIONSET = `${BASE_URL}/promotion_set/create`;
export const URL_GET_PROMOTIONSETS = `${BASE_URL}/promotion_set`;
export const URL_GET_NEWCAMPAIGN_BY_ID = `${BASE_URL}/campaign/id/`;
export const URL_LOCATIONS_STATE = `${BASE_URL}/locations?type=state&offset=0&limit=52`;
export const URL_LOCATIONS_CITY = `${BASE_URL}/locations/search?type=city&name=`;
export const URL_CHAINS = `${BASE_URL}/chains?offset=0&limit=52`;
export const URL_DMA = `${BASE_URL}/dma?offset=0&limit=52`;
export const URL_SAVELOCATION = `${BASE_URL}/promotion_set/wizardcreate/location`;
