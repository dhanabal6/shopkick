import budgetSaga from './sagas/budget.saga';
import getCampaignSaga from './sagas/getCampaignById.saga';
import getPromotionSaga from './sagas/getPromotionSets.saga';
import objectiveSaga from './sagas/objective.saga';
import overviewSaga from './sagas/overview.saga';
import saveLocationSaga from './sagas/saveLocation.saga';
import createPromotionSetSaga from './sagas/savePromotionSet.saga';

import searchSaga from './sagas/search.saga';


export default [
  overviewSaga, objectiveSaga, createPromotionSetSaga, budgetSaga, getPromotionSaga, getCampaignSaga, searchSaga, saveLocationSaga, /* ,savedLocationSaga */
];
