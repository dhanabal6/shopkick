import Chart from 'chart.js';
import classNames from 'classnames';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import styles from './NewCampaignMetrics.css';

class NewCampaignMetrics extends Component {
  static propTypes = {
    startDate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    endDate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    budgetValue: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  };
  static defaultProps = {
    startDate: '',
    endDate: '',
    budgetValue: '',
  };

  componentDidMount() {
    const ctx = document.getElementById('donut');
    new Chart(ctx, {
      type: 'doughnut',
      chart: {
        height: 50,
        width: 100,
      },
      data: {
        labels: ['Total Population', 'Potential Reached'],
        datasets: [{
          label: '# of Votes',
          data: [12, 2],
          backgroundColor: [
            '#dfdfdf',
            '#0097d7',
          ],
          borderColor: [
            'rgba(233,233,233,0)',
          ],
          borderWidth: 1,
        }],
        showInLegend: false,
      },
      options: {
        rotation: Math.PI,
        circumference: Math.PI,
        legend: {
          display: false,
        },
      },
    });
  }

  render() {
    const { startDate, endDate, budgetValue } = this.props;
    return (
      <div className={classNames('fLeft', styles.wrapper)}>
        <h2 className={classNames(styles.title)}>Campaign Metrics</h2>
        <div className={styles.container}>
          <div className={styles.labelWrapper}>
            <label className={styles.labelTitle}>Potential Reach</label>
            <label>87,500 people</label>
          </div>
          <div className={styles.labelWrapper}>
            <label className={styles.labelTitle}>Budget Allocated</label>
            <label>{budgetValue ? `$${budgetValue}` : '-'}</label>
          </div>
          <div className={styles.labelWrapper}>
            <label className={styles.labelTitle}>Campaign Starting</label>
            <label>{startDate ? moment(parseInt(startDate, 10)).format('MM/DD/YYYY') : '-'}</label>
          </div>
          <div className={styles.labelWrapper}>
            <label className={styles.labelTitle}>Campaign Ending</label>
            <label>{endDate ? moment(parseInt(endDate, 10)).format('MM/DD/YYYY') : '-'}</label>
          </div>
        </div>
        <div className={styles.donutWrapper}>
          <canvas id="donut" width="90" height="50" />
        </div>
      </div>
    );
  }
}


export default NewCampaignMetrics;
