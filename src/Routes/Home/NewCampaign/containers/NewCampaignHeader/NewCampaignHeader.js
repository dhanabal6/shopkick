import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { showMessageAlert } from '../../../../../containers/MessageAlert/MessageAlert.actions';
import ProgressMenu from '../../../../../containers/ProgressMenu/ProgressMenu';
import styles from './NewCampaignHeader.css';

class NewCampaignHeader extends Component {
  static propTypes = {
    getDetails: PropTypes.instanceOf(Object),
    dispatch: PropTypes.func,
    updateNavLinks: PropTypes.func,
  };
  static defaultProps = {
    dispatch: f => f,
  };

  constructor() {
    super();
    this.state = {
      totalBars: 9,
      activeBars: 3,
    };
    this.updateActiveCount = this.updateActiveCount.bind(this);
  }

  updateActiveCount(type, count) {
    const { getDetails, dispatch } = this.props;
    const moveLink = getDetails && getDetails.campaignName && getDetails.campaignObjective &&
      getDetails.budgetStartDate && getDetails.budgetEndDate && getDetails.budgetValue;
    if (!getDetails.campaignName || getDetails.campaignName === '') {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign Name',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (!getDetails.campaignObjective || getDetails.campaignObjective === '') {
      dispatch(showMessageAlert({
        message: 'Please Select Campaign Objective',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (getDetails.budgetStartDate === '' || !getDetails.budgetStartDate) {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign Start Date',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (getDetails.budgetEndDate === '' || !getDetails.budgetEndDate) {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign End Date',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (type === 'prev') {
      this.setState({
        activeBars: count - 3,
      });
      this.props.updateNavLinks(count - 3);
    }
    if (moveLink && type === 'next') {
      this.setState({
        activeBars: count + 3,
      });
      this.props.updateNavLinks(count + 3);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { activeNavLink } = this.props;
    if (activeNavLink !== nextProps.activeNavLink) {
      switch (nextProps.activeNavLink) {
        case 'Campaign' : {
          this.setState({
            activeBars: 3,
            title: 'New Campaign',
            subTitle: 'Campaign Objective',
          });
          break;
        }
        case 'Promotion Set' : {
          this.setState({
            activeBars: 6,
            title: 'Promotion Set',
            subTitle: 'Defines where the promotions will be ran and who will see them.',
          });
          break;
        }
        default :
          this.setState({
            activeBars: 9,
            title: 'Promotion',
            subTitle: 'Define what will be seen by the mobile app user',
          });
      }
    }
  }

  render() {
    return (
      <div className={styles.header}>
        <h2 className={'heading2'}>{this.state.title}</h2>
        <h4 className={'heading4'}>{this.state.subTitle}</h4>
        <ProgressMenu
          totalBars={this.state.totalBars}
          activeBars={this.state.activeBars}
          className={styles.progressMenuWrapper}
          updateActiveCount={(type, count) => this.updateActiveCount(type, count)}
        />
      </div>
    );
  }
}

export default NewCampaignHeader;
