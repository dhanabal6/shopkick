import classNames from 'classnames';
import React, { Component } from 'react';
import Button from '../../../../../../components/Button/Button';
import DropdownMenu from '../../../../../../components/DropdownMenu/DropdownMenu';
import Input from '../../../../../../components/Input/Input';
import Textarea from '../../../../../../components/Textarea/Textarea';
import Location from '../../../../../../containers/Location/Location';
import { showMessageAlert } from '../../../../../../containers/MessageAlert/MessageAlert.actions';
import { getCampaignById, savePromotionSet } from '../../../NewCampaign.actions';
import styles from '../NewCampaignMainBody.css';
import LearnMore from './LearnMore';
import TargetUsers from './TargetUsers';

class PromotionSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      promotionName: '',
      promotionType: 'No saved promotions',
      promotionSetTopic: '',
      promotionSetItem: '',
      promotionSetName: '',
      promotionSetDescription: '',
      newPromotionSet: false,
      currentPromotion: 'No promotions',
    };
    this.savePromotion = this.savePromotion.bind(this);
    this.selectSavedPromotion = this.selectSavedPromotion.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    const newCampaignId = localStorage.getItem('newCampaignId');
    if (newCampaignId) dispatch(getCampaignById(newCampaignId));
  }

  savePromotion() {
    const { dispatch } = this.props;
    const newCampaignId = localStorage.getItem('newCampaignId');
    if (this.state.promotionSetName.trim() === '') {
      dispatch(showMessageAlert({
        message: 'Please enter Promotion Set Name',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (this.state.promotionSetTopic === '') {
      dispatch(showMessageAlert({
        message: 'Please select promotion Set Topic',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (this.state.promotionSetItem === '') {
      dispatch(showMessageAlert({
        message: 'Please select promotion Set Item',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    else {
      const locations = [
        {
          type: 'Everyone in this location',
          filter: 'includes',
          category: 'chains',
          value: 'walmart',
        }];
      const userProfile = [
        {
          metric: 'sex',
          value: 'women',
          comparision: 'equals',
          filter: 'must_include',
        }];
      dispatch(savePromotionSet(this.state.promotionSetName, this.state.promotionSetDescription, this.state.promotionSetTopic, this.state.promotionSetItem, parseInt(newCampaignId, 10), locations, userProfile));
      // dispatch(getAllSavedPromotions(userId));
    }
  }

  selectSavedPromotion(item) {
    this.setState({
      promotionSetTopic: item.promo_topic,
      promotionSetItem: item.promo_item,
      promotionSetName: item.name,
      promotionSetDescription: item.details,
    });
  }


  render() {
    return (
      <div className={''}>
        <div className={'clearfix'}>
          <div className={'fLeft'}>
            Current Promotion (0 of 0)
            <br />
            <DropdownMenu
              menuClassName={classNames(styles.promotionDropdown)}
              items={['no promotions']}
              selected={this.state.currentPromotion}
              onSelect={item => this.setState({ currentPromotion: item })}
            />
          </div>
          <Button
            className={classNames('primaryBtn', 'blueBg', 'fRight', styles.saveBtn, styles.newPromotionSetBtn)}
            text={'New Promotion Set'}
            onClick={() => {
              this.setState({
                newPromotionSet: true,
                promotionSetTopic: '',
                promotionSetItem: '',
                promotionSetName: '',
                promotionSetDescription: '',
              });
            }}
          />
        </div>
        <h2 className={classNames('fLeft', styles.title)}>
          <i className={classNames(styles.overviewIcon, 'material-icons')}>feedback</i>
          What Are You Promoting?</h2>
        <span
          className={classNames('fRight', styles.titleDefine)}
        >
          <span className={'tooltipWrapper'}>
            <span>Learn more</span>
            <span className={'tooltip'}>Learn more!</span>
          </span>
        </span>
        <div className={'clearall'} />
        <div
          className={classNames(styles.promotionWrapper, 'clearfix', { [styles.promotionWrapperDisabled]: !this.state.newPromotionSet })}>
          <DropdownMenu
            menuClassName={classNames(styles.savedDropdown, 'fLeft')}
            items={[]}
            selected={this.state.promotionType}
            onSelect={item => this.selectSavedPromotion(item)}
          />
          <LearnMore
            text={''}
            isInfo
            tooltipContent={'info'}
            children={<i
              className={classNames(styles.infoIcon, styles.infoBudgetIcon, 'material-icons')}>info_outline</i>}
            className={classNames(styles.infoBudget, styles.infoInPromotion)}
          />
          <div className={'clearall'}>
          </div>
          <br />
          <div className={styles.promotionForm}>
            <label>Promotion Topic</label>
            <DropdownMenu
              menuClassName={classNames(styles.promotionDropdown)}
              items={['Promoting a Product', 'A movie', 'A holiday', 'A brand', 'A CD', 'A Store']}
              selected={this.state.promotionSetTopic}
              isDisabled={!this.state.newPromotionSet}
              onSelect={item => this.setState({ promotionSetTopic: item })}
            />
          </div>
          <div className={styles.promotionForm}>
            <label>Promotion Set Item</label>
            <DropdownMenu
              menuClassName={classNames(styles.promotionDropdown)}
              items={['Barilla Pasta']}
              selected={this.state.promotionSetItem}
              isDisabled={this.state.promotionSetTopic === ''}
              onSelect={item => this.setState({ promotionSetItem: item })}
            />
          </div>
          <br />
          <hr />
          <br />
          <Input
            autoFocus={false}
            type={'text'}
            label={'Promotion Set Name'}
            placeHolder={'Enter your Promotion Set Name'}
            value={this.state.promotionSetName}
            wrapperClass={styles.promotionInputWrapperClass}
            className={styles.inputField}
            disabled={!this.state.newPromotionSet}
            onChange={(e, value) => this.setState({ promotionSetName: value })}
            message={this.state.promotionSetName === '' && 'Enter your Promotion Set Name'}
          />
          <br />
          <Textarea
            autoFocus={false}
            type={'text'}
            label={'Promotion Set Description'}
            disabled={!this.state.newPromotionSet}
            placeHolder={'Enter Promotion Set Details (optional)'}
            value={this.state.promotionSetDescription}
            wrapperClass={styles.promotionInputWrapperClass}
            className={styles.inputField}
            onChange={(e, value) => this.setState({ promotionSetDescription: value })}
            maxlength={undefined}
          />
          <Button
            className={classNames('primaryBtn', 'blueBg', 'fRight', styles.saveBtn)}
            text={'Save Promotion Set'}
            disabled={!this.state.newPromotionSet}
            onClick={() => {
              this.savePromotion();
            }}
          />
        </div>
        <Location
          Promotionset
        />
        <br />
        <TargetUsers />
      </div>
    );
  }
}

export default PromotionSet;
