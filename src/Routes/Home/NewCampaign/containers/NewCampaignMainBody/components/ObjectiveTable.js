import classNames from 'classnames';
import React, { Component } from 'react';
import styles from '../../../../../../components/Table/Table.css';
import { saveNewCampaignObjective } from '../../../NewCampaign.actions';
import tableStyles from '../NewCampaignMainBody.css';

class ObjectiveTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      objective: this.props.activeObjective,
      type: '',
    };
    this.getTableHeaders = this.getTableHeaders.bind(this);
    this.colGroupWidths = this.colGroupWidths.bind(this);
    this.getTableBody = this.getTableBody.bind(this);
    this.saveObjective = this.saveObjective.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { activeObjective, sendObjectiveDetails } = this.props;
    if (activeObjective !== nextProps.activeObjective) {
      this.setState({
        objective: nextProps.activeObjective,
      }, () => sendObjectiveDetails(this.state.objective));
    }
  }

  getTableHeaders() {
    const { tableHeaders } = this.props;
    return tableHeaders.map((item, i) => (
      <th key={i}>
        {item}
      </th>
    ));
  }

  colGroupWidths() {
    const { columnWidths } = this.props;
    return columnWidths.map((item, i) => (
      <col width={item} key={i} />
    ));
  }

  saveObjective(cell) {
    const { sendObjectiveDetails, dispatch } = this.props;
    this.setState({ objective: cell.value, type: cell.type });
    dispatch(saveNewCampaignObjective(cell.type, cell.value));
    sendObjectiveDetails(cell.value);
  }

  getTableBody() {
    return this.props.tableBody.map((item, i) => (
      <tr key={i}>
        {item.map((cell, j) => (
          <td
            key={j}
            className={(cell.value === this.state.objective) ? tableStyles.active : ''}
            onClick={() => this.saveObjective(cell)}
          >
            <div
              className={(cell.value === this.state.objective) ? tableStyles.active : ''}
            >{cell.value}</div>
          </td>
        ))}
      </tr>
    ));
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <table className={classNames(styles.campaignListTable, tableStyles.objectiveTable)}>
          <colgroup>
            {this.colGroupWidths()}
          </colgroup>
          <thead>
            <tr>
              {this.getTableHeaders()}
            </tr>
          </thead>
          <tbody>
            {this.getTableBody()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ObjectiveTable;
