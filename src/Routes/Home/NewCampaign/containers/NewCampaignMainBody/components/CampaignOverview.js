import classNames from 'classnames';
import React, { Component } from 'react';
import Input from '../../../../../../components/Input/Input';
import Textarea from '../../../../../../components/Textarea/Textarea';
import { getCampaignById, saveNewCampaignOverviewDetails } from '../../../NewCampaign.actions';
import styles from '../NewCampaignMainBody.css';


class CampaignOverview extends Component {
  constructor() {
    super();
    this.state = {
      campaignName: '',
      campaignDetails: '',
      validCampaignName: true,
    };
    this.saveOverview = this.saveOverview.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.newCampaignDetails !== nextProps.newCampaignDetails && nextProps.newCampaignDetails.get('name')) {
      this.setState({
        campaignName: nextProps.newCampaignDetails.get('name'),
        campaignDetails: nextProps.newCampaignDetails.get('details'),
      }, () => this.props.sendOverviewDetails(this.state.campaignName, this.state.campaignDetails));
    }
  }

  componentDidMount() {
    const newCampaignId = localStorage.getItem('newCampaignId');
    if (newCampaignId) this.props.dispatch(getCampaignById(newCampaignId));
  }

  saveOverview(type, val) {
    if (type === 'name') {
      this.setState({ campaignName: val });
      val.trim() === '' ? this.setState({ validCampaignName: false }) : this.props.dispatch(saveNewCampaignOverviewDetails(val, this.state.campaignDetails));
      this.props.sendOverviewDetails(val, this.state.campaignDetails);
    } else {
      this.setState({ campaignDetails: val, validCampaignName: true });
      this.props.dispatch(saveNewCampaignOverviewDetails(this.state.campaignName, val));
      this.props.sendOverviewDetails(this.state.campaignName, val);
    }
  }

  render() {
    return (
      <div className={styles.overviewWrapper}>
        <h2 className={styles.title}>
          <i className={classNames(styles.overviewIcon, 'material-icons')}>visibility</i>Campaign
          Overview
        </h2>
        <div className={styles.container}>
          <Input
            autoFocus={false}
            type={'text'}
            label={'Campaign Name'}
            placeHolder={'Enter Campaign Name'}
            value={this.state.campaignName}
            wrapperClass={styles.inputWrapperClass}
            className={styles.inputField}
            onBlur={(e, value) => {
              this.saveOverview('name', value);
            }}
            error={!this.state.validCampaignName}
            onChange={(e, value) => this.setState({ campaignName: value })}
            message={!this.state.validCampaignName ? 'Enter your Campaign Name' : ''}
            maxlength={undefined}
          />
          <br />
          <Textarea
            autoFocus={false}
            type={'text'}
            label={'Campaign Details'}
            placeHolder={'Enter Campaign Details (optional)'}
            value={this.state.campaignDetails}
            wrapperClass={styles.inputWrapperClass}
            className={styles.inputField}
            onBlur={(e, value) => {
              this.saveOverview('details', value);
            }}
            onChange={(e, value) => this.setState({ campaignDetails: value })}
            maxlength={undefined}
          />
          <br />
        </div>
      </div>
    );
  }
}


export default CampaignOverview;
