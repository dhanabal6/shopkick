import classNames from 'classnames';
import React, { Component } from 'react';
import Button from '../../../../../../components/Button/Button';
import DropdownMenu from '../../../../../../components/DropdownMenu/DropdownMenu';
import Input from '../../../../../../components/Input/Input';
import styles from '../../../../../../containers/Location/Location.css';
import LearnMore from './LearnMore';

class TargetUsers extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.overviewWrapper}>
        <h2 className={classNames('fLeft', styles.title)}>
          <i className={classNames(styles.overviewIcon, 'material-icons')}>supervisor_account</i>
          Target Users</h2>
        <LearnMore
          className={'fRight'}
          text={' Define who you want to see your promotions.'}
        />
        <br />
        <div className={'clearall'} />
        <div className={classNames(styles.container, styles.objectiveWrapper, styles.targetWrapper)}>
          <div className={styles.secondRow}>
            <DropdownMenu
              items={['Must Include', 'Must Exclude']}
              selected={'Must Include'}
              menuClassName={classNames('fLeft', styles.includeLocation)}
              onSelect={() => {}}
            />
            <div className={styles.locationDropdown}>
              <Input
                type={'search'}
                placeHolder={'Type to add more locations'}
                onChange={() => {}}
                className={classNames(styles.headerSearch)}
              />
            </div>
            <div className={styles.browseBtn}>
              <Button
                className={classNames('fLeft', 'browseBtn')}
                text={'Browse'}
                onClick={() => {}}
              />
            </div>
          </div>
          <br />
          <label className={styles.demographicsLabel}>Demographics</label>
          <ul className={styles.demographicsList}>
            <li>Audience Gender is Women</li>
            <li>Age is between 20 and 35</li>
          </ul>
          <div className={classNames(styles.locationBtnWrapper, 'clearfix')}>
            <Button
              className={classNames('fRight', 'primaryBtn', 'blueBg')}
              text={'Save Users'}
              onClick={() => {}}
            />
          </div>
        </div>
      </div>
    )
    ;
  }
}

export default TargetUsers;
