import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import styles from '../NewCampaignMainBody.css';

const LearnMore = ({ className, text, tooltipContent, isInfo, children, ...props }) =>
  (
    <div className={classNames(className, styles.titleDefine)}>
      {text}&nbsp;
      <span className={'tooltipWrapper'}>
        {!isInfo && <span className={'text'}>Learn more</span>}
        {children}
        {tooltipContent && <span className={'tooltip'}>{tooltipContent}</span>}
      </span>
    </div>
  );

export default LearnMore;


LearnMore.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
};

LearnMore.defaultProps = {
  className: '',
  text: '',
  tooltipContent: 'Learn more',
};
