import classNames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';
import DayPicker from 'react-day-picker';
import ReactDOM from 'react-dom';
import DropdownMenu from '../../../../../../components/DropdownMenu/DropdownMenu';
import Input from '../../../../../../components/Input/Input';
import RadioWrapper from '../../../../../../components/RadioWrapper/RadioWrapper';
import { showMessageAlert } from '../../../../../../containers/MessageAlert/MessageAlert.actions';
import '../../../../../../dayPicker.css';
import { saveBudgetDetails } from '../../../NewCampaign.actions';
import styles from '../NewCampaignMainBody.css';
import LearnMore from './LearnMore';

class BudgetSchedule extends Component {
  constructor() {
    super();
    this.state = {
      Date: new Date(),
      startDate: undefined,
      endDate: undefined,
      campaignConstrain: true,
      budgetType: '',
      budgetValue: '',
      currencyType: 'dollar',
      validBudgetValue: true,
      calculatedBudgetVal: '',
      showStartDatePicker: false,
      showEndDatePicker: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.getBudgetType = this.getBudgetType.bind(this);
    this.getBudgetValue = this.getBudgetValue.bind(this);
    this.offClickHandler = this.offClickHandler.bind(this);
  }

  getBudgetType(type) {
    switch (type) {
      case 'campaign_total':
        return 'Campaign Total';
        break;
      case 'amt_per_day':
        return 'Amount per Day';
        break;
      case 'amt_per_week':
        return 'Amount per Week';
        break;
      case 'a_fixed_amt':
        return 'A fixed Amount';
        break;
      case 'amt_per_engagement':
        return 'Amount per Engagement';
        break;
    }
  }

  getBudgetValue(val) {
    const a = moment(this.state.startDate, 'x');
    const b = moment(this.state.endDate, 'x');
    let days;
    let calculatedBudgetVal;
    if (b && a)
      days = b.diff(a, 'days');
    if (this.state.budgetType === 'Amount per Day' && this.state.endDate && this.state.startDate) {
      calculatedBudgetVal = ((days || 1) * parseInt(val, 10));
    }
    if (this.state.budgetType === 'Amount per Week' && this.state.endDate && this.state.startDate) {
      calculatedBudgetVal = (((days || 1) / 7) * parseInt(val, 10));
    }
    return ((calculatedBudgetVal && calculatedBudgetVal.toFixed(2)) || val);
  }

  componentDidMount() {
    document.addEventListener('click', this.offClickHandler, false);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.newCampaignDetails !== nextProps.newCampaignDetails) {
      this.setState({
        budgetType: this.getBudgetType(nextProps.newCampaignDetails.get('budget_type')),
        startDate: nextProps.newCampaignDetails.get('start_date'),
        endDate: nextProps.newCampaignDetails.get('end_date'),
        budgetValue: nextProps.newCampaignDetails.get('budget_amount'),
        calculatedBudgetVal: nextProps.newCampaignDetails.get('budget_amount'),
        campaignConstrain: nextProps.newCampaignDetails.get('budget_sched_management') ? nextProps.newCampaignDetails.get('budget_sched_management') === 'campaign' : true,
      }, () => nextProps.sendBudgetDates(this.state.validBudgetValue ? this.getBudgetValue(this.state.budgetValue) : '', this.state.startDate, this.state.endDate));
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.offClickHandler, false);
  }

  handleChange(type, day) {
    const { dispatch } = this.props;
    const momentDay = moment(day);
    this.setState({
      validBudgetValue: type === 'budget' ? /^\d+(\.\d{1,2})?$/.test(this.state.budgetValue) : this.state.budgetValue,
      startDate: type === 'startDate' ? moment(momentDay).format('x') : this.state.startDate,
      endDate: type === 'endDate' ? moment(momentDay).format('x') : this.state.endDate,
      showStartDatePicker: false,
      showEndDatePicker: false,
    }, () => {
      this.props.sendBudgetDates(this.state.validBudgetValue ? this.getBudgetValue(this.state.budgetValue) : '', this.state.startDate, this.state.endDate);
      const startDate = (this.state.startDate && this.state.startDate !== '') ? parseInt(this.state.startDate, 10) : undefined;
      const endDate = (this.state.endDate && this.state.endDate !== '') ? parseInt(this.state.endDate, 10) : undefined;
      const budgetAmount = this.state.budgetValue !== '' ? parseInt(this.state.budgetValue, 10) : 0;
      if (startDate && endDate) {
        (startDate <= endDate) ? dispatch(saveBudgetDetails(this.state.campaignConstrain ? 'campaign' : 'promo', this.state.budgetType, budgetAmount, this.state.currencyType, startDate, endDate)) : dispatch(showMessageAlert({
          message: 'Please select end date greater than start date',
          visible: true,
          color: '#e53e3d',
        }));
      }
    });
  }

  offClickHandler(event) {
    const domNode = ReactDOM.findDOMNode(this);
    if (!domNode || !domNode.contains(event.target)) {
      this.setState({
        showStartDatePicker: false,
        showEndDatePicker: false,
      });
    }
  }

  render() {
    const currentMonth = this.state.Date.getMonth(),
      currentYear = this.state.Date.getFullYear();
    return (
      <div className={classNames(styles.overviewWrapper)}>
        <h2 className={classNames('fLeft', styles.title)}>
          <i className={classNames(styles.overviewIcon, 'material-icons')}>monetization_on</i>
          Budget and Schedule</h2>
        <LearnMore
          text={'Define budget and when timing Campaign.'}
          tooltipContent={'Learn more'}
          className={'fRight'}
        />
        <div className={'clearall'} />
        <div
          className={classNames(styles.container, styles.objectiveWrapper, styles.budgetWrapper)}
        >
          <LearnMore text={'How will the budget and schedule be managed:'} />
          <br />
          <RadioWrapper
            className={styles.radioWrapper}
            checked={this.state.campaignConstrain}
            text={'Campaign constrains the budget and schedule.'}
            tooltipContent={'Info'}
            onClick={() => this.setState({
              campaignConstrain: true,
              budgetType: undefined,
              budgetValue: '',
            })}
          />
          <RadioWrapper
            className={styles.radioWrapper}
            checked={!this.state.campaignConstrain}
            text={' Promotion constrains the budget and schedule.'}
            tooltipContent={'Info'}
            onClick={() => this.setState({
              campaignConstrain: false,
              budgetType: undefined,
              budgetValue: '',
            })}
          />
          <hr />
          <p>
            All Promotions must start and end within the timeframe set below.
            In addition, the budget spent
            must be less or equal to the budget of the campaign.
          </p>
          <div className={'clearfix'}>
            <div className={'fLeft'}>
              Budget
              <LearnMore
                text={''}
                isInfo
                tooltipContent={'info'}
                children={<i className={classNames(styles.infoIcon, styles.infoBudgetIcon, 'material-icons')}>info_outline</i>}
                className={styles.infoBudget}
              />
              <br />
              <DropdownMenu
                menuClassName={classNames(styles.budgetMenu, 'fLeft')}
                items={this.state.campaignConstrain ? ['Campaign Total', 'Amount per Day', 'Amount per Week'] : ['A fixed Amount', 'Amount Per Engagement']}
                selected={this.state.budgetType}
                onSelect={item => this.setState({ budgetType: item }, () => this.handleChange('budget'))}
              />
            </div>
            <Input
              type={'text'}
              placeHolder={'$'}
              wrapperClass={classNames('fLeft', styles.budgetInputFieldWrapper)}
              disabled={this.state.budgetType === '' || !this.state.budgetType}
              className={classNames(styles.inputField, styles.budgetInputField)}
              value={`$ ${this.state.budgetValue || ''}`}
              onChange={(e, value) =>
                this.setState({ budgetValue: value.replace(/[^\d.]/g, '') }, () => this.handleChange('budget'))
              }
              error={!this.state.validBudgetValue}
              message={!this.state.validBudgetValue ? 'Enter valid amount' : ''}
            />
          </div>
          <div className={'clearfix'}>
            <div className={classNames('fLeft', styles.dateLabel)}>
              <label>Start Date</label><br />
              <div className={classNames('fLeft', styles.dayPickerInputWrapper)}>
                <Input
                  type={'text'}
                  className={styles.dayPickerInput}
                  placeholder={''}
                  value={this.state.startDate ? moment(parseInt(this.state.startDate, 10)).format('L') : 'MM/DD/YYYY'}
                  readonly
                  onClick={() => this.setState({
                    showStartDatePicker: !this.state.showStartDatePicker,
                    showEndDatePicker: false,
                  })}
                />
                <i className={classNames(styles.overviewIcon, 'material-icons')}>date_range</i>
                {this.state.showStartDatePicker && <DayPicker
                  className={styles.daypicker}
                  selectedDays={this.state.startDate}
                  disabledDays={[{
                    from: new Date(currentYear, currentMonth, 1),
                    before: new Date(currentYear, currentMonth, this.state.Date.getDate()),
                  }]}
                  onDayClick={(day, disabled) => !disabled.disabled && this.handleChange('startDate', day)}
                />}
              </div>
            </div>
            <div className={classNames('fLeft', styles.dateLabel)}>
              <label>End Date</label><br />
              <div className={classNames('fLeft', styles.dayPickerInputWrapper)}>
                <Input
                  type={'text'}
                  className={styles.dayPickerInput}
                  placeholder={''}
                  value={this.state.endDate ? moment(parseInt(this.state.endDate, 10)).format('L') : 'MM/DD/YYYY'}
                  readonly
                  onClick={() => this.setState({
                    showEndDatePicker: !this.state.showEndDatePicker,
                    showStartDatePicker: false,
                  })}
                />
                <i className={classNames(styles.overviewIcon, 'material-icons')}>date_range</i>
                {this.state.showEndDatePicker && <DayPicker
                  className={styles.daypicker}
                  selectedDays={this.state.endDate}
                  disabledDays={[{
                    from: new Date(currentYear, currentMonth, 1),
                    before: new Date(currentYear, currentMonth, this.state.Date.getDate()),
                  }]}
                  onDayClick={(day, disabled) => !disabled.disabled && this.handleChange('endDate', day)}
                />}
              </div>
            </div>
          </div>
        </div>

      </div>
    )
      ;
  }
}


export default BudgetSchedule;
