import classNames from 'classnames';
import React, { Component } from 'react';
import styles from '../NewCampaignMainBody.css';
import LearnMore from './LearnMore';
import ObjectiveTable from './ObjectiveTable.js';

class CampaignObjective extends Component {
  constructor() {
    super();
    this.state = {
      activeObjective: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.newCampaignDetails !== nextProps.newCampaignDetails) {
      this.setState({
        activeObjective: nextProps.newCampaignDetails && nextProps.newCampaignDetails.get('objective') && nextProps.newCampaignDetails.get('objective').get('value'),
      });
    }
  }


  render() {
    const { dispatch, sendObjectiveDetails } = this.props;
    return (
      <div className={styles.overviewWrapper}>
        <h2 className={classNames('fLeft', styles.title)}>
          <i className={classNames(styles.overviewIcon, 'material-icons')}>group_work</i>
          Objective</h2>
        <LearnMore
          className={'fRight'}
          text={' Define what you would like to accomplish.'}
        />
        <br />
        <div className={'clearall'} />
        <div className={classNames(styles.container, styles.objectiveWrapper)}>
          <ObjectiveTable
            dispatch={dispatch}
            sendObjectiveDetails={sendObjectiveDetails}
            tableHeaders={['PRODUCT', 'VISITS', 'ENGAGEMENTS']}
            columnWidths={['33.33%', '33.33%', '33.33%']}
            activeObjective={this.state.activeObjective}
            tableBody={[
              [{ value: 'Product Conversion', type: 'product' },
                { value: 'Traffic to the store', type: 'visits' },
                { value: 'Brand Awareness', type: 'engagements' },
              ],
              [{ value: 'Product Category Conversion', type: 'product' },
                { value: 'Extend time at the store', type: 'visits' },
                { value: 'Reach', type: 'engagements' }],
              [{ value: 'Product Awareness', type: 'product' },
                { value: 'Repeat visits to the store', type: 'visits' },
                { value: 'Loyalty', type: 'engagements' }]]}
          />
        </div>
      </div>
    );
  }
}


export default CampaignObjective;
