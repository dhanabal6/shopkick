import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import BudgetSchedule from './components/BudgetSchedule';
import CampaignObjective from './components/CampaignObjective';
import CampaignOverview from './components/CampaignOverview';
import PromotionSet from './components/PromotionSet';
import styles from './NewCampaignMainBody.css';

class NewCampaignMainBody extends Component {
  static propTypes = {
    getDetails: PropTypes.instanceOf(Object),
    dispatch: PropTypes.func,
    updateNavLinks: PropTypes.func,
    sendBudgetDates: PropTypes.func,
    activeNavLink: PropTypes.string,
    userId: PropTypes.number,
    sendObjectiveDetails: PropTypes.func,
    sendOverviewDetails: PropTypes.func,
  };
  static defaultProps = {
    dispatch: f => f,
    updateNavLinks: f => f,
    sendBudgetDates: f => f,
    sendObjectiveDetails: f => f,
    sendOverviewDetails: f => f,
  };

  constructor() {
    super();
    this.state = {
      campaignName: '',
      campaignDetail: '',
      campaignObjective: '',
      budgetValue: '',
      budgetStartDate: '',
      budgetEndDate: '',
    };
  }

  render() {
    const { dispatch, sendBudgetDates, activeNavLink, userId, promotionSetsInfo, sendObjectiveDetails, sendOverviewDetails, newCampaignDetails } = this.props;
    return (
      <div className={classNames('fLeft', styles.wrapper)}>
        {activeNavLink === 'Campaign' &&
        <div>
          <CampaignOverview
            dispatch={dispatch}
            sendOverviewDetails={(campaignName, campaignDetail) => {
              this.setState({
                campaignName,
                campaignDetail,
              });
              sendOverviewDetails(campaignName, campaignDetail);
            }}
            newCampaignDetails={newCampaignDetails}
          />
          <CampaignObjective
            dispatch={dispatch}
            sendObjectiveDetails={(campaignObjective) => {
              this.setState({
                campaignObjective,
              });
              sendObjectiveDetails(campaignObjective);
            }}
            newCampaignDetails={newCampaignDetails}
          />
          <BudgetSchedule
            dispatch={dispatch}
            sendBudgetDates={(budgetValue, budgetStartDate, budgetEndDate) => {
              this.setState({
                budgetValue,
                budgetStartDate,
                budgetEndDate,
              });
              sendBudgetDates(budgetValue, budgetStartDate, budgetEndDate);
            }}
            newCampaignDetails={newCampaignDetails}
          />
        </div>}
        {activeNavLink === 'Promotion Set' && this.state.campaignName && this.state.campaignObjective && this.state.budgetStartDate && this.state.budgetEndDate && this.state.budgetValue &&
        <div>
          <PromotionSet
            dispatch={dispatch}
            userId={userId}
            promotionSetsInfo={promotionSetsInfo}
          />
        </div>}
      </div>
    );
  }
}


export default NewCampaignMainBody;
