import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { showMessageAlert } from '../../../../../containers/MessageAlert/MessageAlert.actions';
import styles from './CampaignNavigator.css';

class CampaignNavigator extends Component {
  static propTypes = {
    getDetails: PropTypes.instanceOf(Object),
    dispatch: PropTypes.func,
    getActiveNavLink: PropTypes.func,
    updateActiveLink: PropTypes.string,
  };
  static defaultProps = {
    dispatch: f => f,
  };

  constructor() {
    super();
    this.state = {
      activeNavLink: 'Campaign',
    };
    this.navLinkClick = this.navLinkClick.bind(this);
  }

  componentDidMount() {
    this.props.getActiveNavLink(this.state.activeNavLink);
  }

  navLinkClick(link) {
    const { getDetails, dispatch } = this.props;
    const moveLink = getDetails && getDetails.campaignName && getDetails.campaignObjective &&
      getDetails.budgetStartDate && getDetails.budgetEndDate && getDetails.budgetValue;
    if (!getDetails.campaignName || getDetails.campaignName === '') {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign Name',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (!getDetails.campaignObjective || getDetails.campaignObjective === '') {
      dispatch(showMessageAlert({
        message: 'Please Select Campaign Objective',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (getDetails.budgetStartDate === '' || !getDetails.budgetStartDate) {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign Start Date',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (getDetails.budgetEndDate === '' || !getDetails.budgetEndDate) {
      dispatch(showMessageAlert({
        message: 'Please enter Campaign End Date',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (getDetails.budgetValue === '' || !getDetails.budgetValue) {
      dispatch(showMessageAlert({
        message: 'Please enter Budget Amount',
        visible: true,
        color: '#e53e3d',
      }));
      return false;
    }
    if (moveLink) {
      this.setState({
        activeNavLink: link,
      });
      this.props.getActiveNavLink(link);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.updateActiveLink !== nextProps.updateActiveLink) {
      this.setState({
        activeNavLink: nextProps.updateActiveLink,
      });
    }
  }

  render() {
    return (
      <div className={classNames('fLeft', styles.wrapper)}>
        <NavigatorList
          listHeaderTitle={'Campaign'}
          navLinkClick={this.navLinkClick}
          isActive={this.state.activeNavLink === 'Campaign'}
          listHeaderVal={'1'}
          subList={['Campaign Overview', 'Objective', 'Budget and Schedule']}
        />
        <NavigatorList
          listHeaderTitle={'Promotion Set'}
          navLinkClick={this.navLinkClick}
          isActive={this.state.activeNavLink === 'Promotion Set'}
          listHeaderVal={'2'}
          subList={['Promotion Set Overview', 'Locations', 'Target Users']}
        />
        <NavigatorList
          listHeaderTitle={'Promotion'}
          navLinkClick={this.navLinkClick}
          isActive={this.state.activeNavLink === 'Promotion'}
          listHeaderVal={'3'}
          subList={['Promotion Overview', 'Format Details', 'Preview', 'Trigger', 'Pricing and Date Range']}
        />
        <NavigatorList
          listHeaderTitle={'Summary'}
          navLinkClick={this.navLinkClick}
          isActive={this.state.activeNavLink === 'Summary'}
          listHeaderVal={'4'}
          subList={[]}
        />
      </div>
    );
  }
}


export default CampaignNavigator;


const NavigatorList = ({ listHeaderTitle, listHeaderVal, subList, isActive, navLinkClick }) => (
  <ul className={styles.navigatorList}>
    <li
      className={classNames('clearfix', styles.listHeader, { [styles.active]: isActive })}
      onClick={() => navLinkClick(listHeaderTitle)}
    >
      <span
        className={classNames('fLeft', styles.listHeaderVal)}
      >{listHeaderVal}</span>
      <span
        className={classNames('fLeft', styles.listHeaderTitle)}
      >{listHeaderTitle}</span>
    </li>
    {subList.map((item, i) => <li key={i} className={styles.subMenuListItem}>{item}</li>)}
  </ul>
);
