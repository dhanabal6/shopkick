import { fromJS } from 'immutable';
import { createReducerFromObject } from '../../../utils/reducerUtils';
import {
  ACTION_SET_CHAINS, ACTION_SET_DMA,
  ACTION_SET_STATE_LOCATIONS, ACTION_SET_CITY_LOCATIONS, ACTION_STORE_PROMOTIONSETS, ACTION_STORE_NEWCAMPAIGN,
} from './NewCampaign.constants';


export const initialState = fromJS({
  promotionSets: [],
  browseLocations: {},
  newCampaignDetails: {
    id: '',
  },
  locations: {},
  chains: {},
  dma: {},
});

const reducerFunctions = {
  [ACTION_STORE_PROMOTIONSETS]: (state, payload) => state.set('promotionSets', fromJS(payload)),
  [ACTION_STORE_NEWCAMPAIGN]: (state, payload) => state.set('newCampaignDetails', fromJS(payload)),
  [ACTION_SET_STATE_LOCATIONS]: (state, payload) => state.set('stateLocations', fromJS(payload.response)),
  [ACTION_SET_CITY_LOCATIONS]: (state, payload) => state.set('cityLocations', fromJS(payload.response)),
  [ACTION_SET_CHAINS]: (state, payload) => state.set('chains', fromJS(payload.response)),
  [ACTION_SET_DMA]: (state, payload) => state.set('dma', fromJS(payload.response)),
};

const newCampaignReducer = createReducerFromObject(reducerFunctions, initialState);
export default newCampaignReducer;
