import React, { Component } from 'react';
import { connect } from 'react-redux';
import CampaignHeader from '../../../containers/CampaignHeader/index';
import styles from '../Dashboard/Dashboard.css';
import { dashboardSelector } from '../Dashboard/Dashboard.selectors';

class AdminPanel extends Component {
  render() {
    const { userInfo } = this.props;
    return (
      <div className={styles.wrapper}>
        <CampaignHeader
          userInfo={userInfo.size > 0 && userInfo.toJS()}
        />
        <div className={styles.cmBodyWrapper}>
          <h2>Coming Soon</h2>
        </div>
      </div>
    );
  }
}


export const mapStateToProps = state => dashboardSelector(state);

export default connect(mapStateToProps)(AdminPanel);
