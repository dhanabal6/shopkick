import classNames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';
import 'react-dates/initialize';
import '../../_datepicker.css';
// import '!style-loader!css-loader!react-dates/lib/css/_datepicker.css'



import { DateRangePicker } from 'react-dates';
// import DayPicker from 'react-day-picker';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import Location from '../../containers/Location/Location';
// import '../../dayPicker.css';
import { getCampaignDetails } from '../../Routes/Home/Dashboard/Dashboard.actions';
import { getAllChain, getAllDMA, getAllLocation } from '../../Routes/Home/NewCampaign/NewCampaign.actions';

import styles from './infoAssistant.css';

class InfoAssistant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Date: new Date(),
      startDate: '',
      endDate: '',
      changeFilter: false,
      focusedInput: null,
    };
    this.handleChange = this.handleChange.bind(this);
        this.onDatesChange = this.onDatesChange.bind(this);
    this.onFocusChange = this.onFocusChange.bind(this);
    this.onInfoSearchTextChange = this.onInfoSearchTextChange.bind(this);
    this.onInfoSearchTextEnter = this.onInfoSearchTextEnter.bind(this);
  }

  componentWillMount() {
    this.setState({
      startDate: this.state.Date,
      endDate: new Date(this.state.Date.getTime() + (7 * 24 * 60 * 60 * 1000)),
    });
  }

onDatesChange({ startDate, endDate }) {

    this.setState({ startDate, endDate });
}

onFocusChange(focusedInput) {
    this.setState({ focusedInput });
}
  // componentWillReceiveProps(nextProps) {
  //   if (this.props.filtersInfo !== nextProps.filtersInfo) {
  //     const { filtersInfo } = nextProps;
  //     this.setState({
  //       startDate: filtersInfo && filtersInfo[0] && filtersInfo[0].from_date !== undefined ? new Date(filtersInfo[0].from_date) : '',
  //       endDate: filtersInfo && filtersInfo[0] && filtersInfo[0].to_date !== undefined ? new Date(filtersInfo[0].to_date) : '',
  //     });
  //   }
  // }

  onInfoSearchTextChange(e, val) {
    this.setState({
      infoSearchText: val,
    });
  }

  onInfoSearchTextEnter() {
    this.props.dispatch(getCampaignDetails(this.state.infoSearchText, this.props.userInfo.id));
  }

  handleChange(day) {
    this.setState({
      startDate: day,
      endDate: new Date(day.getTime() + (7 * 24 * 60 * 60 * 1000)),
    });
  }

  render() {
    console.log(this.state.endDate);
        console.log(this.state.Date);
            console.log(this.state.startDate);
             const { focusedInput, startDate, endDate } = this.state;
    const { filtersInfo, handleApplyFilter, updateState } = this.props;
    const fromDate = filtersInfo && filtersInfo[0] && filtersInfo[0].from_date !== undefined ? new Date(filtersInfo[0].from_date) : 0;
    const toDate = filtersInfo && filtersInfo[0] && filtersInfo[0].to_date !== undefined ? new Date(filtersInfo[0].to_date) : 0;
    const Months = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'];
    // const currentMonth = this.state.startDate.getMonth();
    // const currentYear = this.state.startDate.getFullYear();
    // const startDate = `${Months[this.state.startDate.getMonth()]}\t${this.state.startDate.getDate()},\t${this.state.startDate.getFullYear()}`;
    // const endDate = `${Months[this.state.endDate.getMonth()]}\t${this.state.endDate.getDate()},\t${this.state.endDate.getFullYear()}`;
    const hasFiltersInfoLocations = filtersInfo[0] && filtersInfo[0].locations && filtersInfo[0].locations.length > 0;
    const hasFiltersInfoChains = filtersInfo[0] && filtersInfo[0].chains && filtersInfo[0].chains.length > 0;
    const infoFirstData = filtersInfo && filtersInfo[0];
    return (
      <div className={classNames(
        styles.wrapper,
      )}
      >
        <div className={styles.header}>
          <div className={classNames(styles.headerInfoTitle, 'fLeft')}>
            <i className={'material-icons'}>assignment</i>
            Information Assistant
          </div>
        </div>
        <div
          className={classNames(styles.infoBodyWrapper, { [styles.bgOpaque]: this.state.changeFilter }, 'clearfix')}
        >
          <div className={classNames(styles.filterList, 'fLeft')}>
            {(infoFirstData && infoFirstData.from_date) ?
              <table className={classNames(styles.infoTable, 'fLeft')}>
                <tbody>
                  <tr>
                    <th>From</th>
                    <th>To</th>
                  </tr>
                  <tr>
                    <td>{moment(infoFirstData.from_date).format('LL')}</td>
                    <td>{moment(infoFirstData.to_date).format('LL')}</td>
                    <td>
                      <ul>
                        {hasFiltersInfoChains && infoFirstData.chains.map((item, i) => {
                          if (i < 3) {
                            return (<li key={i}>{item}</li>);
                          }
                          if (i === 3) {
                            return (<span key={i}>&amp; {infoFirstData.chains.length - 2}
                              more </span>);
                          }
                          return null;
                        })}
                      </ul>
                    </td>
                    <td>
                      <ul>
                        {hasFiltersInfoLocations && infoFirstData.locations.map((item, i) => {
                          if (i < 3) {
                            return (<li>{item}</li>);
                          }
                          if (i === 3) {
                            return (
                              <span>&amp; {infoFirstData.locations.length - 2} more </span>);
                          }
                        })}
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table> : <span
                className={styles.infoDefaultText}
              > {fromDate === 0 ? 'Default View: All my current running campaigns' : (`${fromDate.getDate()} ${Months[fromDate.getMonth()]} ${fromDate.getFullYear()}\t To \t${toDate.getDate()} ${Months[toDate.getMonth()]} ${toDate.getFullYear()}`)} </span>}
          </div>
          <div className={classNames(styles.searchFilterWrapper, 'fLeft')}>
            <Input
              type={'text'}
              wrapperClass={styles.inputSearchWrapper}
              placeHolder={'Search for Text'}
              onChange={(e, value) => this.onInfoSearchTextChange('infoSearchText', value)}
              onEnter={this.onInfoSearchTextEnter}
              className={classNames(styles.headerSearch)}
            />
            <Button
              className={classNames('primaryBtn', 'blueBg')}
              text={'Change Filter'}
              onClick={() => {
                this.props.infoAssistActive(true);
                this.props.dispatch(getAllChain());
                this.props.dispatch(getAllDMA());
                this.props.dispatch(getAllLocation());
                this.setState({ changeFilter: true });
              }}
            />
          </div>
        </div>
        {this.state.changeFilter && <div>
          <div className={styles.infoFilterBodyWrapper}>
            <Button
              className={styles.closeIcon}
              text={'X'}
              onClick={() => {
                this.props.infoAssistActive(false);
                this.setState({ changeFilter: false });
              }}
            />
            <h4 className={styles.infoFilterSectionTitle}>Date Range</h4>
            <br />
            <div className={'clearfix'}>
              <div className={classNames('fLeft', styles.dateFilterSection)}>
          <DateRangePicker
          {...this.props}
          // selectedDays={this.state.startDate}
          // onDayClick={day => this.handleChange(day)}
/*          onDatesChange={this.onDatesChange}
          onFocusChange={this.onFocusChange}
                  date={startDate}
                startDate={startDate}
                endDate={endDate}

               startDateId="datepicker_start_home"
                endDateId="datepicker_end_home"                    
                startDatePlaceholderText="Check In"
                endDatePlaceholderText="Check Out"*/

          // endDate={this.state.endDate}
          // onDatesChange={({ startDate, endDate }) => { this.setState({ startDate, endDate })}}
          focusedInput={this.state.focusedInput}
          onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
        />
              </div>

            </div>
            <div className={classNames(styles.infoFilterBodyWrapper, styles.locationContainer)}>
              <br />
              <div className={styles.locationWrapper}>
                <Location />
              </div>
            </div>
          </div>
          <Button
            className={classNames('primaryBtn', 'blueBg', 'fRight', styles.applyBtn)}
            text={'Apply Filter'}
            onClick={() => {
              handleApplyFilter(new Date(this.state.startDate).getTime(), new Date(this.state.endDate).getTime());
              this.props.infoAssistActive(false);
              this.setState({ changeFilter: false });
              updateState(new Date(this.state.startDate).getTime());
            }}
          />
        </div>
        }
      </div>
    );
  }
}

export default InfoAssistant;