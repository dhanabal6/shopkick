import * as React from 'react';
import PropTypes from 'prop-types';
import styles from './MessageAlert.css';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { messageAlertSelector } from './MessageAlert.selector';
import { hideMessageAlert } from './MessageAlert.actions';
import Button from '../../components/Button/Button';

export class MessageAlert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      closeErrorMsg: true,
    };
    this.hide = this.hide.bind(this);
    this.closeMessageAlertListener = this.closeMessageAlertListener.bind(this);
  }

  componentDidMount() {
    setTimeout(() => this.props.dispatch(hideMessageAlert()), 3000);
  }

  hide() {
    this.props.dispatch(hideMessageAlert());
  }

  closeMessageAlertListener(e) {
    if (e.keyCode === 27) {
      this.hide();
    }
  }

  componentWillMount() {
    /* start listening to escape key press to close the MessageAlert box */
    document.addEventListener('keydown', this.closeMessageAlertListener);
  }

  componentWillUnmount() {
    /* stop listening to escape key press after closing the MessageAlert  box */
    document.removeEventListener('keypress', this.closeMessageAlertListener);
  }

  render() {
    const { message, className, color, errors } = this.props;
    const alertType = {
      backgroundColor: color,
    };
    return (
      <div>
        {this.state.closeErrorMsg &&
        <div className={classNames(styles.msgWrapper, className)} style={alertType}>
          <span className={styles.msg}>
            {message}
            {errors}
            <Button
              className={classNames(styles.closeIcon)}
              text={'X'}
              onClick={this.hide}
            />
          </span>
        </div>}
      </div>
    );
  }
}

export const mapStateToProps = state => messageAlertSelector(state);

export default connect(mapStateToProps)(MessageAlert);

MessageAlert.propTypes = {
  message: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

