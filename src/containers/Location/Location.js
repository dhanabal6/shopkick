import classNames from 'classnames';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '../../components/Button/Button';
import DropdownMenu from '../../components/DropdownMenu/DropdownMenu';
import Input from '../../components/Input/Input';
import MapWithAMarker from '../../components/Map/Map';
import UnorderedList from '../../components/UnorderedList/UnorderedList';
import { saveLocation, getAllCityLocation } from '../../Routes/Home/NewCampaign/NewCampaign.actions';
import { newCampaignSelector } from '../../Routes/Home/NewCampaign/NewCampaign.selectors';
import styles from './Location.css';


const selectedLocations = [];
const category = [];
let i = 0;

class Location extends Component {
  constructor() {
    super();
    this.state = {
      toggleLocationsList: false,
      inputValue: '',
      isSelected: {
        value: '',
        category: '',
      },
      filteredLocation: 'Everyone in location*',
      filter: 'include',
      data: {
        promo_set_id: '',
        locations_filters: [],

      },
    };
    this.handleSaveLocationClick = this.handleSaveLocationClick.bind(this);
  }

  /*componentWillUpdate() {
    let isPresent = false;
    this.state.isSelected.value !== '' && selectedLocations.map((item) => {
      if(item.match(this.state.isSelected.value))
        isPresent = true;
    });
    (!isPresent && selectedLocations.push(this.state.isSelected.value) && category.push(this.state.isSelected.category) && this.setState({
      isSelected: {
        value: '',
        category: '',
      },
    }) && i++);
  }*/

  handleSaveLocationClick() {
    selectedLocations.length !== 0 && selectedLocations.forEach((item, index) => {
      const newObject = {
        category: category[index],
        value: selectedLocations[index],
        type: this.state.data.locations_filters[index] !== undefined ?
          this.state.data.locations_filters[index].type :
          this.state.filteredLocation,
        filter: this.state.data.locations_filters[index] !== undefined ?
          this.state.data.locations_filters[index].filter :
          this.state.filter,
      };
      console.log(newObject);
      this.setState({
        data: {
          locations_filters: [
            {
              [index]: newObject,
            },
          ],
        },
      });
      console.log(this.state.data);
    });
    this.props.dispatch(saveLocation(this.state.data));
  }

  render() {
    const { Promotionset, stateLocations, cityLocations, chains, dma } = this.props;
    this.state.isSelected.value !== '' && (selectedLocations.push(this.state.isSelected.value) && category.push(this.state.isSelected.category) && this.setState({
      isSelected: {
        value: '',
        category: '',
      },
    }) && i++);
    const states = [];
    (stateLocations !== undefined && stateLocations.size > 0) &&  stateLocations.toJS().map( item => {
      states.push(item.name);
    });
    const chain = [];
    (chains !== undefined && chains.size > 0) && chains.toJS().map( item => {
      chain.push(item.name);
    });
    const dmas = [];
    (dma !== undefined && dma.size > 0) && dma.toJS().map( item => {
      dmas.push(item.name);
    });
    const data = [
      {
        name: 'State',
        values: states,
      },
      {
        name: 'chains',
        values: chain,
      },
      {
        name: 'DMA',
        values: dmas,
      },
    ];
    return (
      <div className={styles.locationContainer}>
        <h4 className={styles.locationSectionTitle}>Location</h4>
        <br />
        <div className={styles.locationWrapper}>
          <div className={styles.row}>
            <DropdownMenu
              items={['Everyone in location*', 'Only people who live in this location', 'Only location people who are visiting this location']}
              selected={'Everyone in this location'}
              menuClassName={classNames('fLeft', styles.locationCondition)}
              onSelect={(selectedItem) => {
                const newObject = { type: selectedItem };
                this.setState({
                  data: {
                    locations_filters: {
                      [this.state.data.locations_filters.length]: newObject,

                    },
                  },
                });
              }}
            />
            <div className={styles.secondRow}>
              <DropdownMenu
                items={['Include', 'Exclude']}
                selected={'Include'}
                menuClassName={classNames('fLeft', styles.includeLocation)}
                onSelect={(selectedItem) => {
                  let newObject = {};
                  newObject = { filter: selectedItem };
                  const updatedObject = update(this.state.data.locations_filters, newObject);
                  this.setState({
                    data: {
                      locations_filters: {
                        [this.state.data.locations_filters.length]: updatedObject,
                      },
                    },
                  });
                }}
              />
              <div className={styles.locationDropdown}>
                <Input
                  type={'search'}
                  ref="search"
                  placeHolder={'Type to add more locations'}
                  onChange={(e, value) => {
                    this.setState({
                      inputValue: value,
                    });
                    value.length === 2 && this.props.dispatch(getAllCityLocation(value));
                  }
                  }
                  className={classNames(styles.headerSearch)}
                />
                {this.state.inputValue !== '' &&
                <div className={styles.search}>
                  {dma.size > 0 && dma.toJS().map((item) => {
                    if (item.name.toUpperCase().startsWith(this.state.inputValue.toUpperCase())) {
                      return (<div className={styles.searchItems}>
                        <li onClick={() => {
                          this.setState({
                            isSelected: {
                              value: item.name,
                              category: 'Store',

                            },
                            inputValue: '',
                          });
                        }}
                        >
                          <span className={'fLeft'}> {item.name}</span>
                          <span className={'fRight'}> {'Store'} </span>
                        </li>
                      </div>);
                    }
                  })}
                  { chains !== undefined && chains.size > 0 && chains.toJS().map((item) => {
                    if (item.name.toUpperCase().startsWith(this.state.inputValue.toUpperCase())) {
                      return (<div className={styles.searchItems}>
                        <li onClick={() => {
                          this.setState({
                            isSelected: {
                              value: item.name,
                              category: item.type,
                            },
                            inputValue: '',
                          });
                        }}
                        >
                          <span className={'fLeft'}>{item.name}</span>
                          <span className={'fRight'}> {item.type} </span>
                        </li>
                      </div>);
                    }
                  })}
                  {stateLocations !== undefined && stateLocations.size > 0 && stateLocations.toJS().map((item) => {
                    if (item.name.toUpperCase().startsWith(this.state.inputValue.toUpperCase())) {
                      return (<div className={styles.searchItems}>
                        <li onClick={() => {
                          this.setState({
                            isSelected: {
                              value: item.name,
                              category: item.type,
                            },
                            inputValue: '',
                          });
                        }}
                        >
                          <span className={'fLeft'}>{item.name}</span>
                          <span className={'fRight'}> {item.type} </span>
                        </li>
                      </div>);
                    }
                  })}
                  {cityLocations !== undefined && cityLocations.size > 0 && cityLocations.toJS().map((item) => {
                    if (item.name.toUpperCase().startsWith(this.state.inputValue.toUpperCase())) {
                      return (<div className={styles.searchItems}>
                        <li onClick={() => {
                          this.setState({
                            isSelected: {
                              value: item.name,
                              category: item.type,
                            },
                            inputValue: '',
                          });
                        }}
                        >
                          <span className={'fLeft'}>{item.name}</span>
                          <span className={'fRight'}> {item.type} </span>
                        </li>
                      </div>);
                    }
                  })}
                </div>
                }
              </div>
              <div className={styles.browseBtn}>
                <Button
                  className={classNames('fLeft', 'browseBtn')}
                  text={'Browse'}
                  onClick={() => {
                    this.setState({ toggleLocationsList: !this.state.toggleLocationsList });
                  }}
                />
                {this.state.toggleLocationsList && data.length > 0 && <UnorderedList
                  items={data}
                  menuClassName={classNames('fLeft', styles.browseDropDown)}
                  onSelect={(selectedItem, item) => {
                    this.setState({
                      isSelected: {
                        value: item,
                        category: selectedItem,
                      },
                    });
                  }}
                  onOutSideClick={() => this.setState({ toggleLocationsList: false })}
                />}
              </div>
            </div>
          </div>
          {selectedLocations.length > 0 && selectedLocations.map((item, index) => (
            <div className={styles.selectedLocation}>
              <span className={'fLeft'}>{item}</span>
              <span className={'fRight'}>{category[index]}</span>
            </div>
          ))}
          <div className={styles.locationMap}>
            <MapWithAMarker
              containerElement={<div style={{ height: 400 }} />}
              mapElement={<div style={{ height: '100%' }} />}
              selectedLocations={selectedLocations}
            />
          </div>
        </div>
        {Promotionset && <div style={{ marginTop: 15 }}>
          <Button
            className={classNames('fRight', 'primaryBtn', 'blueBg')}
            text={'Save Locations'}
            onClick={() => {
              this.handleSaveLocationClick();
            }}
          />
        </div>}
      </div>
    );
  }
}

export const mapStateToProps = state => newCampaignSelector(state);

export default connect(mapStateToProps)(Location);
