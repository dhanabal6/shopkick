import React, { Component } from 'react';
import classNames from 'classnames';
import styles from './ProgressMenu.css';
import Button from '../../components/Button/Button';


class ProgressMenu extends Component {
  constructor() {
    super();
    this.renderProgressBars = this.renderProgressBars.bind(this);
  }

  renderProgressBars() {
    const { totalBars, activeBars } = this.props;
    return Array(totalBars).fill().map((count, i) => (
      <div key={i} className={classNames('fLeft', styles.bar, { [styles.active]: (i < activeBars) })} />
    ));
  }

  render() {
    const { className, activeBars } = this.props;
    return (
      <div className={classNames(styles.wrapper, className, 'clearfix')}>
        <div className={'fLeft'}>
          {this.renderProgressBars()}
        </div>
        <div className={classNames('fLeft', styles.buttonWrapper)}>
          <Button
            text={'Previous'}
            className={styles.navButton}
            disabled={activeBars === 3}
            onClick={() => this.props.updateActiveCount('prev', activeBars)}
          />
          <Button
            text={'Next'}
            className={styles.navButton}
            onClick={() => this.props.updateActiveCount('next', activeBars)}
          />
        </div>
      </div>
    );
  }
}
export default ProgressMenu;
