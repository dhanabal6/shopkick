import classNames from 'classnames';
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Anchor from '../../components/Anchor/Anchor';
import AvatarWrapper from '../../components/Avatar/AvatarWrapper';
import Button from '../../components/Button/Button';
import { logout } from '../../Routes/Home/Dashboard/Dashboard.actions';
import styles from './styles.css';

class CampaignHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manageProfileVisibility: false,
      userDetails: [],
    };
    this.logOut = this.logOut.bind(this);
    this.offClickHandler = this.offClickHandler.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.offClickHandler, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.offClickHandler, false);
  }

  logOut(e) {
    e.preventDefault();
    const userId = localStorage.getItem('userId');
    this.props.dispatch(logout(userId));
  }

  offClickHandler(event) {
    const domNode = ReactDOM.findDOMNode(this);
    if (!domNode || !domNode.contains(event.target)) {
      this.setState({ manageProfileVisibility: false });
    }
  }

  render() {
    const { userInfo } = this.props;
    return (
      <header>
        <div className={styles.headerContainer}>
          <Anchor
            href="#"
            className={classNames(styles.logo, 'fLeft')}
            onClick={(e) => {
              e.preventDefault();
            }}
          />
          <div
            className={classNames(styles.profile, 'fRight')}
            onClick={() => this.setState({ manageProfileVisibility: !this.state.manageProfileVisibility })}
          >
            <div className={styles.avatar}>
              <AvatarWrapper
                avatarName={userInfo.name}
                avatarUrl={'../../src/images/loginUser.png'}
                avatarClicked={this.state.manageProfileVisibility}
              />
            </div>
            <div
              className={classNames(this.state.manageProfileVisibility ? styles.userdetailsDropdownShow : styles.userdetailsDropdownHide, 'fRight')}
            >
              <h5>User Details</h5>
              <div className={styles.manageUserDetails}>
                <ul>
                  <li>
                    {userInfo.name} <br />
                    {userInfo.company} <br />
                    {userInfo.email} <br />
                    {userInfo.city} <br />
                    {userInfo.state} <br />
                  </li>
                  <li>
                    <a href="#" onClick={() => alert('COMING SOON')}>My
                      Profile Settings</a>
                    <a href="#" onClick={e => this.logOut(e)}>
                      Logout
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <Link to="/newcampaign">
            <Button
              className={classNames(styles.headerBtn, { [styles.active]: window.location.pathname !== '/newcampaign' }, 'fRight')}
              style={window.location.pathname === '/newcampaign' ? { cursor: 'default' } : {}}
              text={window.location.pathname === '/newcampaign' ? 'Creating new Campaign' : 'Create New Campaign'}
            >
              {
                (window.location.pathname !== '/newcampaign') &&
                <i className={classNames('material-icons', styles.createIcon)} />
              }
            </Button>
          </Link>
          <Link to="/adminPanel">
            <Button
              className={classNames(styles.headerBtn, 'fRight')}
              text={'Admin Panel'}
            >
              <i className={classNames('material-icons', styles.settingIcon)} />
            </Button>
          </Link>
          <Link to="/">
            <Button
              className={classNames(styles.headerBtn, 'fRight')}
              text={'Home'}
            >
              <i className={classNames('material-icons', styles.homeIcon)} />
            </Button>
          </Link>
        </div>
      </header>
    );
  }
}

export default connect()(CampaignHeader);
