import { browserHistory } from 'react-router';
import 'whatwg-fetch';
import { decrementLoaderCount, incrementLoaderCount } from '../containers/Loader/Loader.actions';
import { showMessageAlert } from '../containers/MessageAlert/MessageAlert.actions';
import { store } from '../index';

// Global handling for each specific response status code can be directly written under each case.
const GLOBAL_RESPONSE_STATUS_CODE_HANDLERS = {
  // Unauthorized
  401: () => {
    localStorage.removeItem('shopkick-accessToken-remember');
    localStorage.removeItem('userId');
    store.dispatch(decrementLoaderCount());
    store.dispatch(showMessageAlert({
      message: 'Invalid Credentials',
      visible: true,
      color: '#e53e3d',
    }));
    return false;
  },
  503: () => {
    store.dispatch(showMessageAlert({
      message: 'Server down! Please try later',
      visible: true,
      color: '#e53e3d',
    }));
    return false;
  },
};

const redirectToLoginPage = () => {
  localStorage.removeItem('shopkick-accessToken-remember');
  localStorage.removeItem('userId');
  store.dispatch(showMessageAlert({
    message: 'Invalid Credentials',
    visible: true,
    color: '#e53e3d',
  }));
};
/**
 *
 * @param url
 * @param options
 * @param callback
 * @param tokenRequired
 * @returns {Function}
 */

export default function request(url, options, callback, tokenRequired = true) {
  if ((tokenRequired && localStorage.getItem('shopkick-accessToken-remember')) || !tokenRequired) {
    if (options.body) {
      store.dispatch(incrementLoaderCount());
    }
    options.body = typeof options.body !== 'string' ? JSON.stringify(options.body) : options.body;
    const defaultHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    if (tokenRequired) {
      options.headers = {
        Authorization: JSON.parse(localStorage.getItem('shopkick-accessToken-remember')),
      };
    }
    options.headers = options.headers ? Object.assign({}, defaultHeaders, options.headers) : defaultHeaders;
    let statusCode,
      responseStatus,
      responseStatusCode;
    return fetch(url, {
      credentials: 'same-origin',
      ...options,
    })
      .then(checkStatus)
      .then((response) => {
        statusCode = response.status;
        if (statusCode === 401) {
          redirectToLoginPage();
          store.dispatch(decrementLoaderCount());
          return;
        }
        responseStatus = statusCode >= 200 && statusCode < 300 ? 'onSuccess' : 'onError';
        responseStatusCode = statusCode.toString();
        return response.json();
      })
      .then((resp) => {
        if (responseStatus in callback) {
          callback[responseStatus](resp);
        }
        if (responseStatusCode in GLOBAL_RESPONSE_STATUS_CODE_HANDLERS) {
          GLOBAL_RESPONSE_STATUS_CODE_HANDLERS[responseStatusCode](resp);
        }
        // Custom handling for each specific response status code is done based on whether
        // the specific response code keys are present in the callback object or not.
        if (responseStatusCode in callback) {
          callback[responseStatusCode](resp);
        }
        return resp;
      });
  }
  store.dispatch(decrementLoaderCount());
}

/**
 * For 502 we will not get JSON response, hence throw error
 * @param response
 * @returns {*}
 */
function checkStatus(response) {
  if (response.status !== 503) {
    store.dispatch(decrementLoaderCount());
    return response;
  }

  if (response.status === 201) {
    store.dispatch(decrementLoaderCount());
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}
