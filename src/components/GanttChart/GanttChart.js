import classNames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';
import styles from './GanttChart.css';


// TODO :::  code refinement

class GanttChart extends Component {
  constructor() {
    super();
    this.state = {
      campaign_list: [],
      displayYear: moment().format('YYYY'),
      currentDate: moment(),
      tsStart: moment().startOf('year').format('X'),
      tsEnd: moment().endOf('year').format('X'),
    };
    this.handleRowExpand = this.handleRowExpand.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      campaign_list: this.props.campaignsMetrics,
    });
  }

  handleRowExpand(parentItem) {
    this.state.campaign_list.map((item, index) => {
      (item.sk_campaign_id === parentItem.sk_campaign_id) && (item.is_row_open = !item.is_row_open);
    });
    this.setState({});
  }

  handleYearChange(year) {
    const { handleYearChange } = this.props;
    const displayYear = moment(this.state.displayYear, 'YYYY').add(year, 'year');
    const tsStart = displayYear.startOf('year').format('X');
    const tsEnd = displayYear.endOf('year').format('X');
    this.setState({
      displayYear: displayYear.format('YYYY'),
      tsStart,
      tsEnd,
    });
    handleYearChange(tsStart, tsEnd);
  }

  render() {
    const data = this.state.campaign_list;
    const months = [];
    let m = 0;
    for (m = moment.unix(this.state.tsStart); m.isBefore(moment.unix(this.state.tsEnd)); m.add(1, 'months')) {
      months.push(m.format('MMMM'));
    }

    const getCMBox = (date, startDate, endtDate, item) => {
      const servedStart = startDate;
      let servedEnd = endtDate;
      const percentServed = item.percent_served;
      const totalDays = moment(endtDate).diff(startDate, 'days');
      servedEnd = moment(servedStart).add(((percentServed / 100) * totalDays), 'days').format('YYYY-MM-DD');
      let targetServedToDate = 0;
      const targetServePerDay = item.units_contracted / totalDays;

      if (this.state.currentDate.isBefore(startDate)) {
        targetServedToDate = 0;
      } else if (this.state.currentDate.isAfter(endtDate)) {
        targetServedToDate = item.units_served;
      } else {
        const dayServed = moment(this.state.currentDate).diff(startDate, 'days') + 1;
        targetServedToDate = targetServePerDay * dayServed;
      }


      const cssClass = [styles.divSubDay];
      if (moment(date).isSame(startDate)) {
        cssClass.push(styles.divSubDayBox);
        cssClass.push(styles.divSubDayLeft);
      }
      if (moment(date).isSame(endtDate)) {
        cssClass.push(styles.divSubDayBox);
        cssClass.push(styles.divSubDayRight);
      }
      if (moment(date).isBetween(startDate, endtDate)) {
        cssClass.push(styles.divSubDayBox);
      }

      if (
        moment(date).isSame(servedStart) ||
        moment(date).isBetween(servedStart, servedEnd) ||
        moment(date).isSame(servedEnd)) {
        if (item.units_served > targetServedToDate) {
          cssClass.push(styles.bgBlue);
        } else {
          cssClass.push(styles.bgRed);
        }
      }

      return cssClass;
    };

    const createDaysSection = (days, month, item) => {
      const divWidth = (100 / days) + '%';
      let htmlData = [];
      for (let i = 1; i <= days; i++) {
        const date = moment([this.state.displayYear, month, i], 'YYYY-MMMM-DD').format('YYYY-MM-DD');
        let startDate = moment(item.start_date).format('YYYY-MM-DD');
        let endtDate = moment(item.end_date).format('YYYY-MM-DD');

        htmlData.push(
          <div key={date}
               className={moment(date).isSame(this.state.currentDate.format('YYYY-MM-DD')) ? classNames(styles.divDay, styles.todayMarker) : classNames(styles.divDay)}
               style={{ width: divWidth }}>
            <div className={classNames(getCMBox(date, startDate, endtDate, item))}>&nbsp;</div>
          </div>);
      }
      return htmlData;
    };

    const getMenueRow = (index, item) => {
      const rowArrowHtml =
        (item.is_parent ?
           <div className={classNames(styles.mpRow)} key={index}>
             {
               (item.promotions > 1) ?
                 item.is_row_open ?
                   <span className={styles.sideArrow} onClick={() => this.handleRowExpand(item)}>
                                         <i className={classNames(styles.sortUp, 'material-icons')}>
                                             keyboard_arrow_up
                                         </i>
                                     </span>
                   :
                   <span className={styles.sideArrow} onClick={() => this.handleRowExpand(item)}>
                                         <i className={classNames(styles.sideArrow, 'material-icons')}>
                                             keyboard_arrow_down
                                         </i>
                                     </span>
                 :
                 <span className={styles.sideArrow} style={{ visibility: 'hidden' }}>
                                     <i className={classNames(styles.sideArrow, 'material-icons')}>
                                         keyboard_arrow_down
                                 </i>
                                 </span>
             }
             <span className={classNames(styles.dataRow)}>{item.name}</span>
           </div>
           :
           item.is_row_open &&
           (<div className={classNames(styles.mpRow)} key={index}>
                         <span className={styles.sideArrow}>
                             <i className={classNames(styles.sortUp, 'material-icons')} style={{ visibility: 'hidden' }}>
                                 keyboard_arrow_up
                             </i>
                         </span>
             <span className={classNames(styles.dataRow, styles.subMenu)}>{item.name}</span>
           </div>)
        );
      return rowArrowHtml;
    };

    const getProgressBar = (index, item, months) => {
      return (
        item.is_parent ?
          (<div className={classNames(styles.gpRow)} key={index}>
            {months.map((month, index) => {
              const days = moment(month, 'MMMM').daysInMonth();
              return (
                <div className={classNames(styles.divMonth)} key={index}>
                  {createDaysSection(days, month, item)}
                </div>
              );
            })}
          </div>)
          :
          item.is_row_open &&
          (<div className={classNames(styles.gpRow)} key={index}>
            {months.map((month, index) => {
              const days = moment(month, 'MMMM').daysInMonth();
              return (
                <div className={classNames(styles.divMonth)} key={index}>
                  {createDaysSection(days, month, item)}
                </div>
              );
            })}
          </div>)
      );
    };

    return (
      <div className={classNames(styles.schedulePanel)}>
        <div className={classNames(styles.yearSection, 'text-centered')}>
          <span className={classNames(styles.dataRow)}>
            <button
              className={classNames(styles.headerBtn)}
              onClick={() => this.handleYearChange(-1)}
            > &lt; </button>
            <span className={classNames(styles.headerYear)}>{this.state.displayYear}</span>
            <button
              className={classNames(styles.headerBtn)}
              onClick={() => this.handleYearChange(1)}
            > &gt; </button>
          </span>
          <div className={classNames(styles.divMenuPanel)}>
            {data.map((item, index) => {
              return getMenueRow(index, item);
            })}
            <div className={classNames(styles.mpRow)}>
              <span className={styles.sideArrow}>
                <i
                  className={classNames(styles.sortUp, 'material-icons')}
                  style={{ visibility: 'hidden' }}
                >
                  keyboard_arrow_up
                </i>
              </span>
              <span className={classNames(styles.dataRow)} />
            </div>
          </div>
        </div>

        <div className={classNames(styles.monthSection)}>
          <div className={classNames(styles.monthContent)}>
            <div>
              {data && months.map((month, index) => {
                return (
                  <div key={index} className={classNames(styles.divMonth, 'text-centered')}>
                    <span className={classNames(styles.dataRow)}>{month}</span>
                  </div>
                );
              })}
            </div>
            <div>
              <div className={classNames(styles.divGraphPanel)}>
                {data && data.map((item, index) => {
                  return (
                    getProgressBar(index, item, months)
                  );
                })}
                {
                  data && months.map((month, index) => {
                    //const days = moment(month, 'MMMM').daysInMonth();
                    const paddingLeft = (this.state.currentDate.format('DD') * 3.44 - 15) + '%';
                    return (
                      <div className={classNames(styles.divMonth)} key={index}>
                        <div>
                          {
                            (moment().format('YYYY-MMMM') === moment([this.state.displayYear, month], 'YYYY-MMMM').format('YYYY-MMMM')) ?
                              <div
                                className={classNames(styles.textCurrDate)}
                                style={{ paddingLeft }}
                              >
                                Today<span>{this.state.currentDate.format('DD MMM')}</span></div>
                              :
                              <div className={classNames(styles.textCurrDate)}>&nbsp;</div>
                          }

                        </div>
                      </div>
                    );
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default GanttChart;
