import React, { Component } from 'react';
import { Polygon } from 'react-google-maps';
import mapStatePolyfill from './mapStatePolyfill';

class MapPolyfill extends Component {
  constructor() {
    super();
    this.state = {
      polyfilledStates: [],
    };
  }
  componentDidMount() {
    const polyfilledStates = [];
    const self = this;
    if (this.props.location) {
      this.props.location.map((loc, i) => {
        const currentLoc = loc.tags[0];

        Object.keys(mapStatePolyfill).map((key) => {
          if (key.toUpperCase() === currentLoc.value.toUpperCase()) {
            const fill = mapStatePolyfill[key];
            polyfilledStates.push(
              <Polygon key={key} options={{ fillColor: fill.Color, strokeColor: fill.Color, strokeOpacity: 1 }} paths={fill.Coordinates} />,
            );
            self.setState({ polyfilledStates });
          }
        });
      });
    }
    if (this.props.selectedLocations) {
      this.props.selectedLocations.map((loc, i) => {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({ address: loc }, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK) {
            for (let ac = 0; ac < results[0].address_components.length; ac++) {
              const component = results[0].address_components[ac];

              switch (component.types[0]) {
                case 'administrative_area_level_1':
                  Object.keys(mapStatePolyfill).map((key) => {
                    if (key.toUpperCase() === component.long_name.toUpperCase()) {
                      const fill = mapStatePolyfill[key];
                      polyfilledStates.push(
                        <Polygon
                          key={key}
                          options={{
                            fillColor: fill.Color,
                            strokeColor: fill.Color,
                            strokeOpacity: 1,
                          }}
                          paths={fill.Coordinates}
                        />,
                      );
                      self.setState({ polyfilledStates });
                    }
                  });
                  break;
                default:
                  break;
              }
            }
          } else {
            // console.log('Something got wrong' + status);
          }
        });
      });
    }
  }

  render() {
    return (
      <div>
        {this.state.polyfilledStates}
      </div>
    );
  }
}

export default MapPolyfill;
