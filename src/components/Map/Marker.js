import React from 'react';
import { Marker } from 'react-google-maps';

class MarkerComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      marker: [],
      isLable: '',
    };
    this.onHover = this.onHover.bind(this);
    this.onOut = this.onOut.bind(this);
  }

  componentWillUpdate() {
    const marker = [];
    const self = this;
    this.props.location !== undefined && this.props.location.forEach((item, i) => {
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address: item.tags[0].type === 'state' && item.tags[0].value }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          marker.push(
            <Marker
              key={i}
              position={{
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng(),
              }}
              onMouseOver={() => this.onHover(results[0].formatted_address.split(',')[0])}
              onMouseOut={() => this.onOut()}
              label={this.state.isLable === results[0].formatted_address.split(',')[0] && results[0].formatted_address.split(',')[0]}
            />);
          self.setState({ marker });
        } else {
          // alert('Something got wrong' + status);
        }
      });
    });

    this.props.selectedLocations !== undefined && this.props.selectedLocations.forEach((item) => {
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address: item }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          marker.push(
            <Marker
              position={{
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng(),
              }}
              label={results[0].formatted_address.split(',')[0]}
            />);
          self.setState({ marker });
        } else {
          // console.log('Something got wrong' + status);
        }
      });
    });
  }

  onHover(place) {
    this.setState({
      isLable: place,
    });
  }

  onOut() {
    this.setState({
      isLable: '',
    });
  }

  render() {
    return (
      <div>
        {this.state.marker}
      </div>
    );
  }
}

export default MarkerComponent;
