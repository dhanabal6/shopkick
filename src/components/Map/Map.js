import React from 'react';
import {
  withGoogleMap,
  GoogleMap,
} from 'react-google-maps';
import MarkerComponent from './Marker';
import MapPolyfill from './MapPolyfill';

const MapWithAMarker = withGoogleMap(({ location, selectedLocations }) => {
  const MAP_OPTIONS = {
    scrollwheel: false,
  };

  return (
    <GoogleMap
      defaultZoom={4}
      options={MAP_OPTIONS}
      defaultCenter={{ lat: 41.878003, lng: -93.097702 }}
    >

      {
        (location !== false || selectedLocations !== undefined) ? (
          <MapPolyfill
            location={location === false ? undefined : location}
            selectedLocations={selectedLocations}
          />) : (null)
      }
    </GoogleMap>
  );
});

export default MapWithAMarker;
