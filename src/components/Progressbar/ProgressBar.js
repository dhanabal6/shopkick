import classNames from 'classnames';
import propTypes from 'prop-types';
import React from 'react';
import Progress from 'react-progressbar';
import styles from './ProgressBar.css';

const ProgressBar = ({ completed, Xrange, className }) => (
  <div className={classNames(styles.progressbarWrapper, className)}>
    <table className={styles.table}>
      <tbody>
        <tr>
          <td>{Xrange}</td>
          <td>
            <div className={styles.progressbar}>
              <Progress
                completed={completed}
                className={styles.bar}
              />
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>);

ProgressBar.defaultProps = {
  completed: undefined,
  Xrange: '',
  className: '',
};

ProgressBar.propTypes = {
  completed: propTypes.number,
  Xrange: propTypes.string,
  className: propTypes.string,
};

export default ProgressBar;
