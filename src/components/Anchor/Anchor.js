import classNames from 'classnames';
import propTypes from 'prop-types';
import React from 'react';
import styles from './Anchor.css';

const Anchor = ({ child, className, href, classes, ...props }) => (
  <a
    href={href}
    {...props}
    className={classNames(styles.anchor, className, classes.anchor)}
  >
    {child}
  </a>
);

Anchor.defaultProps = {
  child: '',
  className: '',
  href: '#',
  classes: {
    anchor: '',
  },
};

Anchor.propTypes = {
  child: propTypes.node,
  className: propTypes.string,
  href: propTypes.string,
  classes: propTypes.instanceOf(Object),
};

export default Anchor;
