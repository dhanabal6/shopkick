import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import Button from '../../components/Button/Button';
import styles from './TabGroup.css';

export default class TabGroup extends React.Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    className: PropTypes.string,
    activeTab: PropTypes.string,
  };
  static defaultProps = {
    className: '',
    activeTab: '',
    items: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected,
    };
    this.onSelect = this.onSelect.bind(this);
  }

  onSelect(event, selectedItem) {
    event.stopPropagation();
    let update = true;
    if (selectedItem === this.selected) {
      update = false;
    }
    if (update) {
      this.setState({ selected: selectedItem });
      this.setState({ active: true });
    }
    this.props.onSelect(selectedItem);
  }

  render() {
    const { items, className, activeTab } = this.props;

    return (
      <div className={classNames(styles.navbarWapper, className)}>
        {items && items.map((item, i) => (<Button
          text={item.name}
          key={i}
          onClick={event => this.onSelect(event, item)}
          preChildren={
            <i className={classNames('material-icons', item.preIconClass, { [styles.iconActive]: item.name === activeTab })}>
              {item.preIcon}
            </i>
          }
          children={
            <i className={classNames('material-icons', item.postIconClass, { [styles.active]: item.name === activeTab })}>
              {item.postIcon}
            </i>
          }
          className={classNames(styles.navButton,
            { [styles.active]: item.name === activeTab })}
        />))}
      </div>
    );
  }
}
