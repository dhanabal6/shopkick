import React from 'react';
import propTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Checkbox.css';


const Checkbox = ({ className, onClick, checked }) =>
  (
    <span
      className={classNames(className, styles.checkboxIcon, { [styles.borderCheckbox]: !checked })}
      onClick={onClick || ''}
      role={'checkbox'}
      aria-checked={checked}
      tabIndex={0}
    >
      {checked && <span className={classNames(className, styles.checked)} />}
    </span>
  );

Checkbox.defaultProps = {
  className: '',
  onClick: f => f,
  checked: false,
};

Checkbox.propTypes = {
  className: propTypes.string,
  onClick: propTypes.func,
  checked: propTypes.bool,
};

export default Checkbox;
