import HorizontalStackedBar from 'component-horizontal-stacked-bar';
import React from 'react';
import styles from './central-bar_test.css';

const HorizontalBarChart = ({ ages, data }) => {
  const chart = [];
  data['18-24']['value'] !== 0 && ages.map((item, i) =>
    chart.push(
      <div className={styles.barChartStyle} key={i}>
        <div className={styles.barChartStyleAge}> {item} </div>
        <HorizontalStackedBar
          key={i}
          data={[{
            name: 'women',
            value: parseFloat(data[item].women / (data[item].women + data[item].men) * data[item].value),
          },
            {
              name: 'men',
              value: parseFloat(data[item].men / (data[item].women + data[item].men) * data[item].value),
            },
            { name: 'total', value: data[item].value }]}
          generateFillColour={() => ['#005B80', '#ABDDF1', '#FFFF']}
        />
      </div>),
  );

  return (
    <div>
      {chart}
    </div>
  );
};

export default HorizontalBarChart;

