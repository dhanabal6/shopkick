import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import Checkbox from '../../components/Checkbox/Checkbox';
import styles from './UnorderedList.css';

class UnorderedList extends React.Component {
  static propTypes = {
    items: PropTypes.string,
    onSelect: PropTypes.func,
  };
  static defaultProps = {
    items: [],
    onSelect: f => f,
  };

  constructor(props, context) {
    super(props, context);
    const items = [];
    this.props.items.map((item) => {
      const rowItem = {};
      rowItem.name = item.name;
      rowItem.values = item.values;
      rowItem.isOpen = false;
      items.push(rowItem);
    });
    
    this.state = {
      dropdownOpen: false,
      isOpen: '',
      selected: this.props.selected,
      items,
      li_itemChecked: '',
      ul_itemChecked: '',
    };
    this.onSelect = this.onSelect.bind(this);
    this.offClickHandler = this.offClickHandler.bind(this);
    this.rowClickHandler = this.rowClickHandler.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.offClickHandler, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.offClickHandler, false);
  }

  onSelect(event, selectedItem, item) {
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();

    let update = true;
    if (selectedItem === this.selected) update = false;

    if (update) {
      this.setState({ selected: selectedItem });
      this.setState({ active: true });
    }

    this.setState({ dropdownOpen: false });
    this.props.onSelect(selectedItem, item);
  }

  offClickHandler(event) {
    const domNode = ReactDOM.findDOMNode(this);
    if (!domNode || !domNode.contains(event.target)) {
      this.props.onOutSideClick();
    }
  }

  rowClickHandler(item) {
    item.isOpen = !item.isOpen;
    this.setState({});
  }

  render() {
    //const { items } = this.props;
    const menuItems = this.state.items.map((item, i) => (
      <li key={item.name}>
        <span
          className={styles.sortArrows}
          onClick={() => this.rowClickHandler(item)}
          role={'button'}
          tabIndex={0}
        >
          <i className={item.isOpen ? classNames(styles.sortDown, 'material-icons') : classNames(styles.sortUp, 'material-icons')}>
            keyboard_arrow_up
          </i>
        </span>
        {item.name}
        <Checkbox
          checked={this.state.ul_itemChecked === item.name[i]}
          onClick={() => this.setState({ ul_itemChecked: this.state.ul_itemChecked === item.name[i] ? '' : item.name[i] })}
          className={'fRight'}
        />
        <div className={'clearall'} />
        {item.isOpen && item.values !== undefined && <ul className={classNames('dropdown', styles.dropdownMenu)}>
          {item.values.map(x => (
            <li key={x} className={classNames(styles.list)}>
              {x}
              <Checkbox
                checked={this.state.li_itemChecked === x}
                onClick={(e) => { this.onSelect(e, item.name, x); this.setState({ li_itemChecked: this.state.li_itemChecked === x ? '' : x }); }}
                className={'fRight'}
              />
            </li>
          ))}
        </ul>
        }
      </li>
    ));
    return (
      <ul className={classNames('dropdown', styles.dropdownMenu)}>
        {menuItems}
      </ul>
    );
  }
}

export default UnorderedList;
