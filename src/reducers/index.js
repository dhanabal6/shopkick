import { combineReducers } from 'redux';
import messageAlertReducer from '../containers/MessageAlert/MessageAlert.reducer';
import loaderReducer from '../containers/Loader/Loader.reducer';
import loginReducer from '../Routes/Login/Login.reducer';
import dashboardReducer from '../Routes/Home/Dashboard/Dashboard.reducer';
import newCampaignReducer from '../Routes/Home/NewCampaign/NewCampaign.reducer';

export default function createReducer(asyncReducers) {
  const appReducer = combineReducers({
    messageAlert: messageAlertReducer,
    loader: loaderReducer,
    login: loginReducer,
    dashboard: dashboardReducer,
    newCampaign: newCampaignReducer,
    ...asyncReducers,
  });

  return (state, action) => appReducer(state, action);
}
