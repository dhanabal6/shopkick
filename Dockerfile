FROM node:9.3.0

MAINTAINER Rajib Chowdhury <rajib.c@shopkick.com>

# application placed into /opt/app
# RUN mkdir -p Shopkick-CM-Frontend
# WORKDIR Shopkick-CM-Frontend

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn

# Install and configure `serve`.
# RUN npm shrinkwrap --dev
RUN npm install -g serve
CMD serve -s __build__
EXPOSE 5000

# Install all dependencies of the current project.
COPY package.json package.json
COPY package-lock.json package-lock.json
# COPY npm-shrinkwrap.json npm-shrinkwrap.json
RUN npm install

# Copy all local files into the image.
COPY . .

# Build for development.
RUN npm run start